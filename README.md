czech-timetable
===============

Data covertor from Czech timetable format(s) to GTFS timetable format.

Features
--------

* Imports JDFs and processes them into a GTFS SQLite database and a
  set of GTFS files.

Known gaps and issues
---------------------

* Only JDF support for now, no support for train timetables, they are
  in a different format.

* Partial implementation of stop geolocation by looking up information
  in PBF format map data of Czech Republic. Still needs a lot of
  improvement and is currently unusable for practical purposes.

Usage
-----

*   Fetch raw data:

    ```bash
    export SOURCE_MAP= # provide URL for a pbf file with Czech map data
    export SOURCE_BUS= # provide URL for a zip file with bus routes in zipped JDFs
    export SOURCE_TRACK_TOWN= # provide URL for a zip file with town track routes in zipped JDFs
    toolbox/run make raw-data-get
    ```

    Data URLs are not provided with this tool due to lack of clarity
    in data licensing. To my knowledge, all of the necessary data is
    open data, but i am not a lawyer.

*   Create the SQLite DB:

    ```bash
    toolbox/run make data-build
    ```

*   Create the GTFS files:

    ```bash
    toolbox/run make gtfs-build
    ```

Working with subset of the data
-------------------------------

*   It is possible to filter which agencies' data is exported to
    GTFS. E.g. this way only data for DPMB (the main Brno public
    transport company) will be exported:

    ```bash
    export EXPORT_GTFS_FLAGS='--agency CZ25508881_1'
    toolbox/run make gtfs-build
    ```

    The `--agency` option can be provided multiple times.

*   If you don't even want to import all data from JDF files into the
    intermediary SQLite database, the `data-build` step also supports
    `--agency` option, which can also be given multiple times if
    desired.

    ```bash
    export IMPORT_JDF_FLAGS='--agency CZ25508881_1'
    toolbox/run make data-build
    ```
