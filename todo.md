TODOs
-----

Bugs:

Features:

* Import train timetables

Refactorings:

* Clean up variable names in main.rs

  * Copy-paste errors

  * `db_path_arg` vs. `db_arg`

* More consistent/predictable module names

* Cleaner code in jdf_import
