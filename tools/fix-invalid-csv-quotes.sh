#!/bin/bash

set -euo pipefail

while [ -n "${1:-}" ]; do
    FILE="$1"
    # Some JDF files are invalid CSV: strings can contain double quotes
    # which are not escaped, so the CSV parsing breaks completely. This is
    # a heuristic to try and find those broken quotes and escape them (by
    # doubling, which is the CSV way). The heuristic is unfortunately not
    # bullet proof, but at least something.
    sed -E 's/([^,;])"([^,;])/\1""\2/g' < "$FILE" > "$FILE.fixed"
    mv "$FILE.fixed" "$FILE"
    shift
done
