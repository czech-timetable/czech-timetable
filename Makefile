.PHONY: build clean data-build data-clean gtfs-build raw-data-get raw-data-clean raw-data-timetable-clean raw-data-validate test test-unit

.DEFAULT_GOAL := build
SHELL := /bin/bash
ROOT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

MKDIR_DIRS := data
JDF_ZIP_DIRS := raw-data/jdf-bus raw-data/jdf-track-town

RAW_DATA_TMP ?= 1
BUILD_FLAGS ?= --release
RUN_RELEASE_BIN ?= ./target/release/czech-timetable
RUN_RELEASE_BIN_FLAGS ?=
# RUN_RELEASE_BIN_FLAGS ?= --debug

EXPORT_GTFS_FLAGS ?=
IMPORT_JDF_FLAGS ?=
SOURCE_BUS ?=
SOURCE_TRACK_TOWN ?=
SOURCE_MAP ?=

### TOOLBOX ###

toolbox-build:
	cd toolbox && \
	buildah bud -t localhost/czech-timetable-toolbox:latest . && \
	podman tag localhost/czech-timetable-toolbox:latest localhost/czech-timetable-toolbox:$$(date "+%Y_%m_%d")

toolbox-clean:
	podman rmi localhost/czech-timetable-toolbox:latest

### BUILD ###

build:
	cargo build $(BUILD_FLAGS)

clean:
	cargo clean

### TEST ###

test: test-unit

test-unit:
	cargo test

### GTFS ###

gtfs-build: data-build data/gtfs

data/gtfs: data/czech-timetable.sqlite
	rm -rf data/gtfs; \
	$(RUN_RELEASE_BIN) $(RUN_RELEASE_BIN_FLAGS) export-gtfs --db "$<" --gtfs-dir "$@" \
		$(EXPORT_GTFS_FLAGS)

### DATA ###

data-build: raw-data-get data/czech-timetable.sqlite

data/czech-timetable.sqlite: data $(JDF_ZIP_DIRS) | build
	set -e; \
	rm -f data/czech-timetable.sqlite; \
	ALL_JDF_DIRS="$$(find $(JDF_ZIP_DIRS) -mindepth 1 -type d | sort -V)" && \
	$(RUN_RELEASE_BIN) $(RUN_RELEASE_BIN_FLAGS) create-db --db "$@" && \
	for DIR in $$ALL_JDF_DIRS; do \
		echo "Importing $$DIR."; \
		$(RUN_RELEASE_BIN) $(RUN_RELEASE_BIN_FLAGS) import-jdf --db "$@" --jdf-dir "$$DIR" \
			$(IMPORT_JDF_FLAGS); \
	done; \
	echo "Minifying DB."; \
	$(RUN_RELEASE_BIN) $(RUN_RELEASE_BIN_FLAGS) minify-db --db "$@"

data-clean:
	rm -r data/* || true

### RAW DATA ###

raw-data-get: raw-data/jdf-bus raw-data/jdf-track-town
# raw-data-get: raw-data/jdf-bus raw-data/jdf-track-town raw-data/czech_republic.osm.pbf

raw-data-clean:
	rm -r raw-data/* || true

raw-data-timetable-clean:
	rm raw-data/jdf-bus.zip; rm raw-data/jdf-track-town.zip; rm -r $(JDF_ZIP_DIRS) || true

raw-data/czech_republic.osm.pbf:
	if [ -z "$(SOURCE_MAP)" ]; then \
		echo "SOURCE_MAP not set, can't fetch data."; \
		exit 1; \
	fi; \
	curl -o "$@" "$(SOURCE_MAP)"

$(JDF_ZIP_DIRS): % : %.zip
	rm -r "./$@" || true && \
	unzip -DD -d "$@" "$<" && \
	for JDFBATCH in "$@/"*.zip; do \
		JDFDIR="$$(dirname "$$JDFBATCH")/$$(basename -s .zip "$$JDFBATCH")"; \
		unzip -DD -d "$$JDFDIR" "$$JDFBATCH"; \
		./tools/fix-invalid-csv-quotes.sh "$$JDFDIR"/*; \
	done

raw-data/jdf-bus.zip: | raw-data
	if [ -z "$(SOURCE_BUS)" ]; then \
		echo "SOURCE_BUS not set, can't fetch data."; \
		exit 1; \
	fi; \
	curl -Lo $@ "$(SOURCE_BUS)"

raw-data/jdf-track-town.zip: | raw-data
	if [ -z "$(SOURCE_TRACK_TOWN)" ]; then \
		echo "SOURCE_TRACK_TOWN not set, can't fetch data."; \
		exit 1; \
	fi; \
	curl -Lo $@ "$(SOURCE_TRACK_TOWN)"

raw-data-validate: | build raw-data-get
	set -e; \
	ALL_JDF_DIRS="$$(find $(JDF_ZIP_DIRS) -mindepth 1 -type d | sort -V)" && \
	for DIR in $$ALL_JDF_DIRS; do \
		echo "Validating $$DIR."; \
		$(RUN_RELEASE_BIN) $(RUN_RELEASE_BIN_FLAGS) validate-jdf --jdf-dir "$$DIR" || FAILED=1 ; \
	done; \
	exit $$FAILED

### UTILITY ###

$(MKDIR_DIRS):
	mkdir $@

raw-data:
	if [ "$(RAW_DATA_TMP)" = 1 ]; then \
		mkdir -p /tmp/czech-timetable/raw-data ; \
		ln -s /tmp/czech-timetable/raw-data raw-data; \
	else \
		mkdir $@; \
	fi
