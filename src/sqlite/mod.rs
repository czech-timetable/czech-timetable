use CzttResult;
use std::path::Path;
use rusqlite::{Connection, types::ToSql};

const DB_SCHEMA_SQL: &'static str = include_str!("db-schema.sql");

pub fn connect(db_path: &Path) -> CzttResult<Connection> {
    debug!("Connecting to database");
    let conn = Connection::open(db_path)?;
    foreign_keys_on(&conn)?;
    Ok(conn)
}

pub fn create_db(db_path: &Path) -> CzttResult<()> {
    debug!("Creating database");
    if db_path.exists() {
        return Err(format!("Database {:?} already exists.", db_path).into());
    }

    let conn = Connection::open(db_path)?;
    begin(&conn)?;
    create_db_schema(&conn)?;
    end(&conn)?;
    debug!("Database created");
    Ok(())
}

pub fn foreign_keys_on(conn: &Connection) -> CzttResult<()> {
    conn.execute("PRAGMA foreign_keys = ON;", &[] as &[&dyn ToSql]).map(|_| ()).map_err(|e| e.into())
}

pub fn foreign_keys_off(conn: &Connection) -> CzttResult<()> {
    conn.execute("PRAGMA foreign_keys = OFF;", &[] as &[&dyn ToSql]).map(|_| ()).map_err(|e| e.into())
}

pub fn vacuum(conn: &Connection) -> CzttResult<()> {
    conn.execute("VACUUM;", &[] as &[&dyn ToSql]).map(|_| ()).map_err(|e| e.into())
}

pub fn begin(c: &Connection) -> CzttResult<()> {
    c.execute("BEGIN;", &[] as &[&dyn ToSql]).map(|_| ()).map_err(|e| e.into())
}

pub fn end(c: &Connection) -> CzttResult<()> {
    c.execute("END;", &[] as &[&dyn ToSql]).map(|_| ()).map_err(|e| e.into())
}

fn create_db_schema(conn: &Connection) -> CzttResult<()> {
    conn.execute_batch(DB_SCHEMA_SQL).and(Ok(())).map_err(|e| e.into())
}
