PRAGMA foreign_keys = ON;

CREATE TABLE agency (
    -- official spec allows NULL for agency_id in single-agency data,
    -- but we expect dealing with multi-agency data, so require it
    agency_id TEXT NOT NULL PRIMARY KEY,
    agency_name TEXT NOT NULL,
    agency_url TEXT NOT NULL,
    agency_timezone TEXT NOT NULL,
    agency_lang TEXT,
    agency_phone TEXT,
    agency_fare_url TEXT,
    agency_email TEXT
);

CREATE TABLE stops (
    stop_id TEXT NOT NULL PRIMARY KEY,
    stop_code TEXT,
    stop_name TEXT NOT NULL,
    stop_desc TEXT,
    stop_lat REAL NOT NULL,
    stop_lon REAL NOT NULL,
    zone_id TEXT,
    stop_url TEXT,
    location_type INTEGER,
    parent_station TEXT,
    stop_timezone TEXT,
    wheelchair_boarding INTEGER
);

CREATE INDEX stops_by_name ON stops(stop_name);

CREATE TABLE routes (
    route_id TEXT NOT NULL PRIMARY KEY,
    -- require agency_id as we're dealing with multi-agency data
    agency_id TEXT NOT NULL,
    route_short_name TEXT NOT NULL,
    route_long_name TEXT NOT NULL,
    route_desc TEXT,
    route_type INTEGER NOT NULL,
    route_url TEXT,
    route_color TEXT,
    route_text_color TEXT,
    route_sort_order TEXT,

    FOREIGN KEY (agency_id) REFERENCES agency(agency_id)
);

CREATE INDEX routes_by_agency ON routes(agency_id);

CREATE TABLE trips (
    route_id TEXT NOT NULL,
    service_id TEXT NOT NULL,
    trip_id TEXT NOT NULL PRIMARY KEY,
    trip_headsign TEXT,
    trip_short_name TEXT,
    direction_id INTEGER,
    block_id TEXT,
    shape_id TEXT,
    wheelchair_accessible INTEGER,
    bikes_allowed INTEGER,

    FOREIGN KEY (route_id) REFERENCES routes(route_id) ON DELETE CASCADE,
    FOREIGN KEY (shape_id) REFERENCES shapes(shape_id)
);

CREATE INDEX trips_by_route ON trips(route_id);
CREATE INDEX trips_by_service ON trips(service_id);
CREATE INDEX trips_by_shape ON trips(shape_id);

CREATE TABLE stop_times (
    trip_id TEXT NOT NULL,
    arrival_time TEXT NOT NULL,
    departure_time TEXT NOT NULL,
    stop_id TEXT NOT NULL,
    stop_sequence TEXT NOT NULL,
    stop_headsign TEXT,
    pickup_type INTEGER,
    drop_off_type INTEGER,
    shape_dist_traveled REAL,
    timepoint INTEGER,

    FOREIGN KEY (trip_id) REFERENCES trips(trip_id) ON DELETE CASCADE,
    FOREIGN KEY (stop_id) REFERENCES stops(stop_id)
);

CREATE INDEX stop_times_by_stop ON stop_times(stop_id);
CREATE INDEX stop_times_by_trip ON stop_times(trip_id);

CREATE TABLE calendar (
    service_id TEXT NOT NULL PRIMARY KEY,
    monday INTEGER NOT NULL,
    tuesday INTEGER NOT NULL,
    wednesday INTEGER NOT NULL,
    thursday INTEGER NOT NULL,
    friday INTEGER NOT NULL,
    saturday INTEGER NOT NULL,
    sunday INTEGER NOT NULL,
    start_date TEXT NOT NULL,
    end_date TEXT NOT NULL
);

CREATE INDEX calendar_by_service_id ON calendar(service_id);

CREATE TABLE calendar_dates (
    service_id TEXT NOT NULL,
    date TEXT NOT NULL,
    exception_type INTEGER NOT NULL
);

CREATE INDEX calendar_dates_by_service_id ON calendar_dates(service_id);

CREATE TABLE fare_attributes (
    fare_id TEXT NOT NULL PRIMARY KEY,
    price TEXT NOT NULL,
    currency_type TEXT NOT NULL,
    payment_method INTEGER NOT NULL,
    transfers INTEGER NOT NULL,
    agency_id TEXT NOT NULL,
    transfer_duration INTEGER
);

CREATE TABLE fare_rules (
    fare_id TEXT NOT NULL,
    route_id TEXT,
    origin_id TEXT,
    destination_id TEXT,
    contains_id TEXT,

    FOREIGN KEY (fare_id) REFERENCES fare_attributes(fare_id),
    FOREIGN KEY (route_id) REFERENCES routes(route_id)
);

CREATE INDEX fare_rules_by_fare ON fare_rules(fare_id);
CREATE INDEX fare_rules_by_route ON fare_rules(route_id);
CREATE INDEX fare_rules_by_origin ON fare_rules(origin_id);
CREATE INDEX fare_rules_by_destination ON fare_rules(destination_id);
CREATE INDEX fare_rules_by_contains ON fare_rules(contains_id);

CREATE TABLE shapes (
    shape_id TEXT NOT NULL PRIMARY KEY,
    shape_pt_lat REAL NOT NULL,
    shape_pt_lon REAL NOT NULL,
    shape_pt_sequence INTEGER NOT NULL,
    shape_dist_traveled REAL
);

CREATE TABLE frequencies (
    trip_id TEXT NOT NULL,
    start_time TEXT NOT NULL,
    end_time TEXT NOT NULL,
    headway_secs INTEGER NOT NULL,
    exact_times INTEGER,

    FOREIGN KEY (trip_id) REFERENCES trips(trip_id)
);

CREATE INDEX frequencies_by_trip ON frequencies(trip_id);

CREATE TABLE transfers (
    from_stop_id TEXT NOT NULL,
    to_stop_id TEXT NOT NULL,
    transfer_type INTEGER NOT NULL,
    min_transfer_time INTEGER,

    FOREIGN KEY (from_stop_id) REFERENCES stops(stop_id),
    FOREIGN KEY (to_stop_id) REFERENCES stops(stop_id)
);

CREATE INDEX transfers_by_from_stop ON transfers(from_stop_id);
CREATE INDEX transfers_by_to_stop ON transfers(to_stop_id);

CREATE TABLE feed_info (
    feed_publisher_name TEXT NOT NULL,
    feed_publisher_url TEXT NOT NULL,
    feed_lang TEXT,
    feed_start_date TEXT,
    feed_end_date TEXT,
    feed_version TEXT
);
