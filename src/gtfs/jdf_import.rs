use chrono::{Datelike, NaiveDate, NaiveTime, Weekday};
use CzttResult;
use holiday_cz;
use jdf::*;
use rusqlite::{Connection, types::ToSql};
use sqlite::{begin, end};

const TIME_FORMAT: &'static str = "%H:%M:%S";
const DATE_FORMAT: &'static str = "%Y%m%d";
const MAX_STOP_SEQUENCE: i64 = 9999;

pub fn import_jdf(conn: &Connection, jdf: &Jdf, agency_ids: Option<Vec<String>>)
                  -> CzttResult<()> {
    if let Some(ids) = agency_ids {
        if ! jdf_has_requested_agency(&jdf, &ids) {
            info!("Skipping JDF, it does not contain any requested agency.");
            return Ok(());
        }
    }

    import_agencies(conn, jdf)?;
    import_stops(conn, jdf)?;
    import_routes(conn, jdf)?;
    import_trips(conn, jdf)?;
    import_stop_times(conn, jdf)?;
    Ok(())
}

fn jdf_has_requested_agency(jdf: &Jdf, requested_ids: &Vec<String>) -> bool {
    for agency in &jdf.agencies {
        for id in requested_ids {
            if &agency.unique_id() == id {
                return true;
            }
        }
    }
    false
}

fn import_agencies(conn: &Connection, jdf: &Jdf) -> CzttResult<()> {
    begin(conn)?;
    for agency in &jdf.agencies {
        let agency_id = agency.unique_id();
        if row_exists(conn, "agency", "agency_id", &agency_id) {
            debug!("Skipping agency '{}', already imported.", &agency_id);
            continue;
        }

        conn.execute(
            "INSERT INTO agency (agency_id, agency_name, agency_url, agency_timezone)
                         VALUES (?, ?, ?, 'Europe/Prague')",
            &[&agency_id as &dyn ToSql,
              &agency.name,
              agency.www.as_ref().unwrap_or(&String::new())])?;
        debug!("Imported agency '{}'.", &agency_id);
    }
    end(conn)
}

fn import_stops(conn: &Connection, jdf: &Jdf) -> CzttResult<()> {
    begin(conn)?;
    for stop in &jdf.stops {
        let stop_id = stop.unique_id();
        let readable_name = stop.readable_name();
        if row_exists(conn, "stops", "stop_id", &stop_id) {
            debug!("Skipping stop '{}', already imported.", &readable_name);
            continue;
        }

        conn.execute(
            "INSERT INTO stops (stop_id, stop_name, stop_lat, stop_lon)
                         VALUES (?, ?, 0.0, 0.0)",
            &[&stop_id as &dyn ToSql,
              &readable_name])?;
        debug!("Imported stop '{}'.", &readable_name);
    }
    end(conn)
}

fn import_routes(conn: &Connection, jdf: &Jdf) -> CzttResult<()> {
    begin(conn)?;
    for route in &jdf.routes {
        let route_uni_id = route.unique_id();
        let agency_uni_id = jdf.get_agency(&route.agency_id, &route.agency_sub_id)
            .expect(&format!("Couldn't find JDF agency {}_{} for route {}.",
                             &route.agency_id, &route.agency_sub_id, &route.route_id))
            .unique_id();

        // TODO: lookup RouteExt and fetch short name
        let short_name = match jdf.get_route_ext(&route.route_id, &route.route_sub_id) {
            Some(re) => re.label.clone(),
            None => route.route_id.clone(),
        };
        let long_name = format!("{} {}", &route.route_id, &route.name);

        if row_exists(conn, "routes", "route_id", &route_uni_id) {
            debug!("Skipping route '{}', already imported.", &route.name);
            continue;
        }

        let route_type = match route.vehicle_type.to_lowercase().as_str() {
            "a" => 3, "e" => 0, "l" => 6, "m" => 1, "p" => 4, "t" => 3, _ => 3,
        };
        conn.execute(
            "INSERT INTO routes (route_id, agency_id, route_short_name, route_long_name, route_type)
                         VALUES (?, ?, ?, ?, ?)",
            &[&route_uni_id as &dyn ToSql, &agency_uni_id, &short_name, &long_name, &route_type])?;
        debug!("Imported route '{}'.", &route.name);
    }
    end(conn)
}

fn import_trips(conn: &Connection, jdf: &Jdf) -> CzttResult<()> {
    begin(conn)?;
    for trip in &jdf.trips {
        let route = jdf.get_route(&trip.route_id, &trip.route_sub_id)
            .expect(&format!("Couldn't find JDF route {}_{} for trip {}.",
                             &trip.route_id, &trip.route_sub_id, &trip.trip_id));
        let route_uni_id = route.unique_id();
        let trip_uni_id = trip.unique_id(&route_uni_id);

        if row_exists(conn, "trips", "trip_id", &trip_uni_id) {
            debug!("Skipping trip '{}', already imported.", &trip_uni_id);
            continue;
        }

        let service_uni_id = import_service(conn, jdf, &route, &trip)?;

        conn.execute(
            "INSERT INTO trips (route_id, service_id, trip_id)
                         VALUES (?, ?, ?)",
            &[&route_uni_id as &dyn ToSql, &service_uni_id, &trip_uni_id])?;
        debug!("Imported trip '{}'.", &trip_uni_id);
    }
    end(conn)
}

fn import_service(conn: &Connection, jdf: &Jdf, route: &Route, trip: &Trip)
                  -> CzttResult<String> {
    // not starting a transaction here because we assume this is
    // called from import_trips
    let trip_cals = jdf.get_trip_calendars(&route.route_id, &route.route_sub_id, trip.trip_id);
    let cal_label = if trip_cals.len() > 0 {
        let cal_label = &trip_cals[0].cal_label;
        for tc in &trip_cals {
            assert!(&tc.cal_label == cal_label, "Different trip calendar labels for the same trip.");
        }
        cal_label
    } else {
        ""
    };

    let service_uni_id = format!("{}_{}", &route.unique_id(), &cal_label);

    if row_exists(conn, "calendar", "service_id", &service_uni_id)
        || row_exists(conn, "calendar_dates", "service_id", &service_uni_id) {
            debug!("Skipping service '{}', already imported.", &service_uni_id);
            return Ok(service_uni_id);
    }

    let codes: Vec<&Code> = {
        let mut some_codes = vec![
            &trip.attr1, &trip.attr2, &trip.attr3, &trip.attr4, &trip.attr5,
            &trip.attr6, &trip.attr7, &trip.attr8, &trip.attr9, &trip.attr10 ];
        some_codes.retain(|c| c.is_some());
        some_codes.iter()
            .map(|c| c.as_ref().expect("Error unwrapping code even though filtered for is_some"))
            .map(|c| jdf.get_code(&c).expect("Couldn't find referenced code"))
            .collect()
    };

    let wd = jdf_codes_to_gtfs_weekdays(&codes);

    // TODO even- and odd-week trips handling
    // TODO public holiday handling
    // TODO make sure that any go-date and no-go-dates collisions prioritize no-go-dates
    let (dates_only, ranges, exceptions) = jdf_trip_cals_to_gtfs_service(&trip_cals);
    let date_range: Option<(&NaiveDate, &NaiveDate)>;

    match ranges.len() {
        // no ranges, use route dates for calendar
        0 => date_range = Some((&route.valid_from, &route.valid_to)),
        // single range, use the range for calendar
        1 => date_range = Some((&ranges[0].from, &ranges[0].to)),
        // multiple ranges, don't use calendar, expand ranges to dates
        _ => {
            let mut range_dates: Vec<NaiveDate> = vec![];
            for range in ranges {
                range_dates.append(&mut expand_jdf_date_range(Some(range.from), Some(range.to)));
            }
            range_dates.retain(|d| date_fulfills_weekday_conditions(d, &wd));
            for date in range_dates {
                conn.execute(
                    "INSERT INTO calendar_dates (service_id, date, exception_type)
                                 VALUES (?, ?, ?)",
                    &[&service_uni_id as &dyn ToSql, &fdate(&date), &1_u8])?;
            }
            debug!("Imported multiple calendar date ranges for service '{}'.", &service_uni_id);

            date_range = None
        },
    }

    let holiday_exceptions: Vec<GtfsCalDate>;
    if dates_only || date_range.is_none() {
        debug!("Service '{}' is dates only, skipping calendar.", &service_uni_id);
        holiday_exceptions = vec![];
    } else {
        let range = date_range.expect("Date range is none even though guarded by check");
        conn.execute(
            "INSERT INTO calendar (service_id, monday, tuesday, wednesday, thursday,
                                   friday, saturday, sunday, start_date, end_date)
                         VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            &[&service_uni_id as &dyn ToSql, &wd.mon, &wd.tue, &wd.wed, &wd.thu, &wd.fri, &wd.sat,
              &wd.sun, &fdate(&range.0), &fdate(&range.1)])?;
        debug!("Imported calendar for service '{}'.", &service_uni_id);

        // given this is a normal calendar service, make sure to
        // generate CZ holiday dates as exceptions (either go or don't
        // go, depending on info in &wd)
        let mut holidays = holiday_cal_dates(&wd, &range.0, &range.1);

        // avoid collisions with exception dates already specified in the JDF exceptions
        holidays.retain(|h| ! cal_dates_contain_date(&exceptions, &h.date));
        holiday_exceptions = holidays;
    }

    for exc in exceptions.into_iter().chain(holiday_exceptions.into_iter()) {
        conn.execute(
            "INSERT INTO calendar_dates (service_id, date, exception_type)
                         VALUES (?, ?, ?)",
            &[&service_uni_id as &dyn ToSql, &fdate(&exc.date), &exc.exc_type])?;
        debug!("Imported calendar dates for service '{}'.", &service_uni_id);
    }

    Ok(service_uni_id)
}

fn holiday_cal_dates(wd: &GtfsWeekdays, start: &NaiveDate, end: &NaiveDate) -> Vec<GtfsCalDate> {
    let exc_type = match wd.hol {
        1 => 1,
        _ => 2,
    };

    holiday_cz::holidays_within(start, end).into_iter()
        .map(|h_date| GtfsCalDate { date: h_date, exc_type: exc_type })
        .collect()
}

fn cal_dates_contain_date(haystack: &[GtfsCalDate], needle: &NaiveDate) -> bool {
    for cal_date in haystack {
        if &cal_date.date == needle {
            return true;
        }
    }
    false
}

fn import_stop_times(conn: &Connection, jdf: &Jdf) -> CzttResult<()> {
    begin(conn)?;
    for trip_stop in &jdf.trip_stops {
        let trip_compound_id = format!(
            "{}_{}_{}", &trip_stop.route_id, &trip_stop.route_sub_id, &trip_stop.trip_id);

        // Get *some* time to use as fallback if all else fails. If
        // fallback time is empty, the data is invalid.
        let fallback_time_opt =
            trip_stop.departure_time.as_ref()
            .or(trip_stop.arrival_time.as_ref()
                .or(trip_stop.max_departure_time.as_ref()
                    .or(trip_stop.min_arrival_time.as_ref())));
        if fallback_time_opt.is_none() {
            debug!("All times are empty for stop {} of trip {}, skipping.",
                   &trip_stop.rate_number, &trip_compound_id);
            continue;
        }
        let fallback_time = fallback_time_opt
            .expect("Failed fallback_time unwrap in import_stop_times() despite is_none() check.");

        let route_uni_id = jdf.get_route(&trip_stop.route_id, &trip_stop.route_sub_id)
            .expect(&format!("Couldn't find JDF route {}_{} for trip stop of trip {}.",
                             &trip_stop.route_id, &trip_stop.route_sub_id, &trip_compound_id))
            .unique_id();
        let trip_uni_id = jdf
            .get_trip(&trip_stop.route_id, &trip_stop.route_sub_id, trip_stop.trip_id)
            .expect(&format!("Couldn't find JDF trip {} for trip stop.", &trip_compound_id))
            .unique_id(&route_uni_id);
        let stop_uni_id = jdf.get_stop(&trip_stop.stop_id)
            .expect(&format!("Couldn't find JDF stop {} for trip {}.",
                             &trip_stop.stop_id, &trip_compound_id))
            .unique_id();

        // NOTE: Checking for duplicate insertion here would be nice
        // to have, but is low priority, because duplicate trip_stop
        // entry is not expected. It would mean there's something
        // broken in the input data.

        let arrival_time: String = ftime(trip_stop.arrival_time.as_ref()
            .unwrap_or(trip_stop.min_arrival_time.as_ref()
                        .unwrap_or(fallback_time)));
        let departure_time: String = ftime(trip_stop.departure_time.as_ref()
            .unwrap_or(trip_stop.max_departure_time.as_ref()
                       .unwrap_or(fallback_time)));

        // trips with even id numbers are "back" direction and their
        // stop sequence needs to be reversed
        let stop_seq = match trip_stop.trip_id % 2 {
            0 => MAX_STOP_SEQUENCE - trip_stop.rate_number,
            _ => trip_stop.rate_number,
        };

        conn.execute(
            "INSERT INTO stop_times (trip_id, arrival_time, departure_time, stop_id, stop_sequence)
                         VALUES (?, ?, ?, ?, ?)",
            &[&trip_uni_id as &dyn ToSql, &arrival_time, &departure_time, &stop_uni_id, &stop_seq])?;
        debug!("Imported stop_time {}_{}_{}.", &trip_uni_id, &trip_stop.rate_number, &stop_uni_id);
    }
    end(conn)
}

fn ftime(time: &NaiveTime) -> String {
    time.format(TIME_FORMAT).to_string()
}

fn fdate(time: &NaiveDate) -> String {
    time.format(DATE_FORMAT).to_string()
}

fn row_exists(conn: &Connection, table: &str, column: &str, value: &str) -> bool {
    // cannot have '?' in places of tables and columns, use format
    let mut prep_q = conn.prepare(
        &format!("SELECT {} FROM {} WHERE {} = ?", column, table, column))
        .expect("Error when preparing SQL query");
    let result = prep_q.query_map(&[&value], |_row| Ok(1))
        .expect("Error when executing SQL query")
        .count() != 0;
    result
}

fn date_fulfills_weekday_conditions(date: &NaiveDate, wd: &GtfsWeekdays) -> bool {
    if holiday_cz::is_holiday(date) && wd.hol == 0 {
        return false;
    }

    match date.weekday() {
        Weekday::Mon => if wd.mon == 1 { return true },
        Weekday::Tue => if wd.tue == 1 { return true },
        Weekday::Wed => if wd.wed == 1 { return true },
        Weekday::Thu => if wd.thu == 1 { return true },
        Weekday::Fri => if wd.fri == 1 { return true },
        Weekday::Sat => if wd.sat == 1 { return true },
        Weekday::Sun => if wd.sun == 1 { return true },
    }

    false
}

fn jdf_codes_to_gtfs_weekdays(codes: &[&Code]) -> GtfsWeekdays {
    let mut wd = GtfsWeekdays { mon: 0, tue: 0, wed: 0, thu: 0, fri: 0, sat: 0, sun: 0, hol: 0 };
    for code in codes {
        match code.code_meaning.as_str() {
            "X" => { wd.mon = 1; wd.tue = 1; wd.wed = 1; wd.thu = 1; wd.fri = 1 },
            "+" => { wd.sun = 1; wd.hol = 1 },
            "1" => { wd.mon = 1 },
            "2" => { wd.tue = 1 },
            "3" => { wd.wed = 1 },
            "4" => { wd.thu = 1 },
            "5" => { wd.fri = 1 },
            "6" => { wd.sat = 1 },
            "7" => { wd.sun = 1 },
            _ => {},
        }
    }
    wd
}

fn jdf_trip_cals_to_gtfs_service(trip_cals: &[&TripCalendar])
                                 -> (bool, Vec<GtfsCalRange>, Vec<GtfsCalDate>) {
    let mut ranges: Vec<GtfsCalRange> = vec![];
    let mut dates: Vec<GtfsCalDate> = vec![];
    let mut only_dates = false;

    for tc in trip_cals {
        match tc.cal_type.as_ref().map(|ct| ct.as_str()) {
            // goes
            Some("1") => { ranges.push(jdf_trip_cal_to_gtfs_cal_range(&tc)) },
            // goes also
            Some("2") => { dates.append(&mut jdf_trip_cal_to_gtfs_cal_dates(&tc)) },
            // goes only
            Some("3") => { only_dates = true;
                           dates.append(&mut jdf_trip_cal_to_gtfs_cal_dates(&tc)) },
            // doesn't go
            Some("4") => { dates.append(&mut jdf_trip_cal_to_gtfs_cal_dates(&tc)) },
            _ => {},
        }
    }
    (only_dates, ranges, dates)
}

fn jdf_trip_cal_to_gtfs_cal_range(tc: &TripCalendar) -> GtfsCalRange {
    let fallback_date = tc.from_date.or(tc.to_date)
        .expect("Calendar date range type 1 (goes) requires either from or to date, or both.");
    GtfsCalRange {
        from: tc.from_date.unwrap_or(fallback_date),
        to: tc.to_date.unwrap_or(fallback_date),
    }
}

fn jdf_trip_cal_to_gtfs_cal_dates(tc: &TripCalendar) -> Vec<GtfsCalDate> {
    let dates = expand_jdf_date_range(tc.from_date, tc.to_date);

    match tc.cal_type.as_ref().map(|ct| ct.as_str()) {
        Some("2") => { dates.into_iter().map(|d| GtfsCalDate { date: d, exc_type: 1 }).collect() },
        Some("3") => { dates.into_iter().map(|d| GtfsCalDate { date: d, exc_type: 1 }).collect() },
        Some("4") => { dates.into_iter().map(|d| GtfsCalDate { date: d, exc_type: 2 }).collect() },
        _ => { vec![] },
    }
}

fn expand_jdf_date_range(from: Option<NaiveDate>, to: Option<NaiveDate>) -> Vec<NaiveDate> {
    let mut dates: Vec<NaiveDate> = vec![];

    if to.is_none() {
        match from {
            Some(d) => { dates.push(d.clone()) },
            None => {},
        }
    } else {
        let f = from.expect("No 'from' date found for from-to range.");
        let t = to.expect("No 'to' date found for from-to range.");
        assert!(f <= t, "The 'from' date is later than 'to' date.");

        let mut current = f;
        while current <= t {
            dates.push(current);
            current = current.succ();
        }
    }
    dates
}

struct GtfsCalRange {
    pub from: NaiveDate,
    pub to: NaiveDate,
}

struct GtfsCalDate {
    pub date: NaiveDate,
    // 1 = goes, 2 = doesn't go
    pub exc_type: u8,
}

struct GtfsWeekdays {
    // 0 = doesn't go, 1 = goes
    pub mon: u8,
    pub tue: u8,
    pub wed: u8,
    pub thu: u8,
    pub fri: u8,
    pub sat: u8,
    pub sun: u8,
    pub hol: u8,
}
