use csv;
use CzttResult;
use rusqlite::{Connection, Row, types::ToSql};
use std::io::{BufWriter, Write};
use std::fs;
use std::path::Path;

type CsvWriter = csv::Writer<BufWriter<fs::File>>;

pub fn export(conn: &Connection, dir_path: &Path, agency_ids: Option<Vec<String>>)
              -> CzttResult<()> {
    let all_ids = all_agency_ids(&conn)?;
    let used_agency_ids: Vec<String> = match agency_ids {
        Some(ids) => {
            assert_valid_ids(&ids, &all_ids)?;
            ids
        },
        None => all_ids,
    };

    fs::create_dir(dir_path)?;
    for agency_id in used_agency_ids {
        let agency_path = dir_path.join(&agency_id);
        fs::create_dir(&agency_path)?;
        export_agency(&conn, &agency_id, &agency_path.join("agency.txt"))?;
        export_stops(&conn, &agency_id, &agency_path.join("stops.txt"))?;
        export_routes(&conn, &agency_id, &agency_path.join("routes.txt"))?;
        export_trips(&conn, &agency_id, &agency_path.join("trips.txt"))?;
        export_stop_times(&conn, &agency_id, &agency_path.join("stop_times.txt"))?;
        export_calendar(&conn, &agency_id, &agency_path.join("calendar.txt"))?;
        export_calendar_dates(&conn, &agency_id, &agency_path.join("calendar_dates.txt"))?;
    }
    Ok(())
}

fn all_agency_ids(conn: &Connection) -> CzttResult<Vec<String>> {
    let mut prep_stmt = conn.prepare("SELECT agency_id FROM agency ORDER BY agency_id")?;
    let mut rows = prep_stmt.query(&[] as &[&dyn ToSql])?;
    let mut agency_ids: Vec<String> = vec![];
    while let Some(r) = rows.next()? {
        agency_ids.push(r.get::<usize, Option<String>>(0)?.ok_or("Error fetching agency id")?)
    }
    Ok(agency_ids)
}

fn assert_valid_ids(ids_to_validate: &Vec<String>, all_ids: &Vec<String>) -> CzttResult<()> {
    for id in ids_to_validate {
        if ! all_ids.contains(id) {
            return Err(format!("Agency with ID '{}' not found in the DB.", id).into());
        }
    }
    Ok(())
}

fn export_agency(conn: &Connection, agency_id: &str, csv_path: &Path) -> CzttResult<()> {
    debug!("Exporting agency.txt for {}", agency_id);
    let table = "agency";
    let columns = "agency_id, agency_name, agency_url, agency_timezone,
                agency_lang, agency_phone, agency_fare_url, agency_email";
    let where_ = "agency_id = :agency_id";
    let order_by = "agency_id";
    export_table(conn, csv_path, table, columns, where_, order_by, agency_id,
                 |w, r| Ok(w.write_record(
                     &[&get_s(&r, 0)?, &get_s(&r, 1)?, &get_s(&r, 2)?, &get_s(&r, 3)?,
                       &get_s(&r, 4)?, &get_s(&r, 5)?, &get_s(&r, 6)?, &get_s(&r, 7)?])?))
}

fn export_stops(conn: &Connection, agency_id: &str, csv_path: &Path) -> CzttResult<()> {
    debug!("Exporting stops.txt for {}", agency_id);
    let table = "stops";
    let columns =
        "stop_id, stop_code, stop_name, stop_desc, stop_lat, stop_lon, zone_id, stop_url,
         location_type, parent_station, stop_timezone, wheelchair_boarding";
    let where_ = "stop_id in \
                  (SELECT DISTINCT stop_id FROM stop_times WHERE trip_id IN \
                  (SELECT DISTINCT trip_id FROM trips WHERE route_id IN \
                  (SELECT DISTINCT route_id FROM routes WHERE agency_id = :agency_id)))";
    let order_by = "stop_id";
    export_table(conn, csv_path, table, columns, where_, order_by, agency_id,
                 |w, r| Ok(w.write_record(
                     &[&get_s(&r, 0)?, &get_s(&r, 1)?, &get_s(&r, 2)?, &get_s(&r, 3)?,
                       &get_f(&r, 4)?, &get_f(&r, 5)?, &get_s(&r, 6)?, &get_s(&r, 7)?,
                       &get_i(&r, 8)?, &get_s(&r, 9)?, &get_s(&r, 10)?, &get_i(&r, 11)?])?))
}

fn export_routes(conn: &Connection, agency_id: &str, csv_path: &Path) -> CzttResult<()> {
    debug!("Exporting routes.txt for {}", agency_id);
    let table = "routes";
    let columns =
        "route_id, agency_id, route_short_name, route_long_name, route_desc, route_type,
         route_url, route_color, route_text_color, route_sort_order";
    let where_ = "agency_id = :agency_id";
    let order_by = "route_id";
    export_table(conn, csv_path, table, columns, where_, order_by, agency_id,
                 |w, r| Ok(w.write_record(
                     &[&get_s(&r, 0)?, &get_s(&r, 1)?, &get_s(&r, 2)?, &get_s(&r, 3)?,
                       &get_s(&r, 4)?, &get_i(&r, 5)?, &get_s(&r, 6)?, &get_s(&r, 7)?,
                       &get_s(&r, 8)?, &get_s(&r, 9)?])?))
}

fn export_trips(conn: &Connection, agency_id: &str, csv_path: &Path) -> CzttResult<()> {
    debug!("Exporting trips.txt for {}", agency_id);
    let table = "trips";
    let columns =
        "route_id, service_id, trip_id, trip_headsign, trip_short_name, direction_id, block_id,
         shape_id, wheelchair_accessible, bikes_allowed";
    let where_ = "route_id in \
                  (SELECT DISTINCT route_id FROM routes WHERE agency_id = :agency_id)";
    let order_by = "trip_id";
    export_table(conn, csv_path, table, columns, where_, order_by, agency_id,
                 |w, r| Ok(w.write_record(
                     &[&get_s(&r, 0)?, &get_s(&r, 1)?, &get_s(&r, 2)?, &get_s(&r, 3)?,
                       &get_s(&r, 4)?, &get_i(&r, 5)?, &get_s(&r, 6)?, &get_s(&r, 7)?,
                       &get_i(&r, 8)?, &get_i(&r, 9)?])?))
}

fn export_stop_times(conn: &Connection, agency_id: &str, csv_path: &Path) -> CzttResult<()> {
    debug!("Exporting stop_times.txt for {}", agency_id);
    let table = "stop_times";
    let columns =
        "trip_id, arrival_time, departure_time, stop_id, stop_sequence, stop_headsign,
         pickup_type, drop_off_type, shape_dist_traveled, timepoint";
    let where_ = "trip_id in \
                  (SELECT DISTINCT trip_id FROM trips WHERE route_id in \
                  (SELECT DISTINCT route_id FROM routes WHERE agency_id = :agency_id))";
    let order_by = "trip_id, stop_sequence";
    export_table(conn, csv_path, table, columns, where_, order_by, agency_id,
                 |w, r| Ok(w.write_record(
                     &[&get_s(&r, 0)?, &get_s(&r, 1)?, &get_s(&r, 2)?, &get_s(&r, 3)?,
                       &get_s(&r, 4)?, &get_s(&r, 5)?, &get_i(&r, 6)?, &get_i(&r, 7)?,
                       &get_i(&r, 8)?, &get_i(&r, 9)?])?))
}

fn export_calendar(conn: &Connection, agency_id: &str, csv_path: &Path) -> CzttResult<()> {
    debug!("Exporting calendar.txt for {}", agency_id);
    let table = "calendar";
    let columns =
        "service_id, monday, tuesday, wednesday, thursday, friday, saturday, sunday,
         start_date, end_date";
    let where_ = "service_id in \
                  (SELECT DISTINCT service_id FROM routes WHERE agency_id = :agency_id)";
    let order_by = "service_id";
    export_table(conn, csv_path, table, columns, where_, order_by, agency_id,
                 |w, r| Ok(w.write_record(
                     &[&get_s(&r, 0)?, &get_i(&r, 1)?, &get_i(&r, 2)?, &get_i(&r, 3)?,
                       &get_i(&r, 4)?, &get_i(&r, 5)?, &get_i(&r, 6)?, &get_i(&r, 7)?,
                       &get_s(&r, 8)?, &get_s(&r, 9)?])?))
}

fn export_calendar_dates(conn: &Connection, agency_id: &str, csv_path: &Path) -> CzttResult<()> {
    debug!("Exporting calendar_dates.txt for {}", agency_id);
    let table = "calendar_dates";
    let columns = "service_id, date, exception_type";
    let where_ = "service_id in \
                  (SELECT DISTINCT service_id FROM routes WHERE agency_id = :agency_id)";
    let order_by = "service_id, date";
    export_table(conn, csv_path, table, columns, where_, order_by, agency_id,
                 |w, r| Ok(w.write_record(&[&get_s(&r, 0)?, &get_s(&r, 1)?, &get_i(&r, 2)?])?))
}

fn export_table<F>(conn: &Connection, csv_path: &Path, table: &str, columns: &str, where_: &str,
                   order_by: &str, agency_id: &str, write_fn: F) -> CzttResult<()>
    where F: Fn(&mut CsvWriter, &Row) -> CzttResult<()> {

    let mut prep_stmt = conn.prepare(&format!(
        "SELECT {} FROM {} WHERE {} ORDER BY {}", columns, table, where_, order_by))?;
    let mut w = csv_writer(csv_path, columns)?;
    let mut rows = prep_stmt.query_named(&[(":agency_id", &agency_id)])?;
    while let Some(r) = rows.next()? {
        write_fn(&mut w, &r)?;
    }
    Ok(())
}

fn csv_writer(csv_path: &Path, columns: &str) -> CzttResult<CsvWriter> {
    let mut w = BufWriter::new(fs::File::create(csv_path)?);
    w.write(format!("{}\n", columns.replace(" ", "").replace("\n", "")).as_bytes())?;
    Ok(csv::WriterBuilder::new()
        .quote_style(csv::QuoteStyle::Always)
        .from_writer(w))
}

fn get_s(r: &Row, idx: usize) -> CzttResult<String> {
    Ok(r.get::<usize, Option<String>>(idx)?.unwrap_or("".to_string()))
}

fn get_f(r: &Row, idx: usize) -> CzttResult<String> {
    Ok(r.get::<usize, Option<f64>>(idx)?.map(|f| format!("{}", f)).unwrap_or("".to_string()))
}

fn get_i(r: &Row, idx: usize) -> CzttResult<String> {
    Ok(r.get::<usize, Option<i64>>(idx)?.map(|i| format!("{}", i)).unwrap_or("".to_string()))
}
