use CzttResult;
use base64;
use chrono::{Duration, Utc};
use sqlite;
use std::iter::Iterator;
use rusqlite::{Connection, types::ToSql};
use sqlite::{begin, end};

const DATE_FORMAT: &'static str = "%Y%m%d";

pub fn minify(conn: &Connection, reindex: bool, reindex_agencies_b: bool, reindex_prefix: &str,
              time_limits: bool, max_history: u32, max_future: u32) -> CzttResult<()> {
    if time_limits {
        // will delete also stop_times via cascade delete
        delete_outdated_calendars_and_trips(conn, max_history, max_future)?;
        delete_empty_routes(conn)?;
    }
    if reindex {
        sqlite::foreign_keys_off(conn)?;
        if reindex_agencies_b {
            reindex_agencies(conn, reindex_prefix)?;
        }
        reindex_stops(conn, reindex_prefix)?;
        reindex_routes(conn, reindex_prefix)?;
        reindex_trips(conn, reindex_prefix)?;
        reindex_services(conn, reindex_prefix)?;
        sqlite::foreign_keys_on(conn)?;
    }
    sqlite::vacuum(conn)?;
    Ok(())
}

fn delete_outdated_calendars_and_trips(conn: &Connection, max_history: u32, max_future: u32)
                                       -> CzttResult<()> {
    begin(conn)?;
    let window_start = (Utc::today().naive_utc() - Duration::days(max_history as i64))
        .format(DATE_FORMAT).to_string();
    let window_end = (Utc::today().naive_utc() + Duration::days(max_future as i64))
        .format(DATE_FORMAT).to_string();

    let mut prep_stmt = conn.prepare(
        "SELECT service_id FROM calendar
            WHERE end_date < :window_start OR start_date > :window_end;")?;
    let delete_service_ids: Vec<String> = prep_stmt.query_map_named::<String, _>(
        &[(":window_start", &window_start), (":window_end", &window_end)],
        |r| r.get::<usize, String>(0))?
        .map(|res| res.expect("Failed to fetch service IDs to delete")).collect();
    for del_service_id in delete_service_ids {
        debug!("Deleting service {} and its trips and stop_times (out of time window)",
               &del_service_id);
        // also deletes stop_times via cascade
        conn.execute("DELETE FROM trips WHERE service_id = ?;", &[&del_service_id])?;
        conn.execute("DELETE FROM calendar WHERE service_id = ?;", &[&del_service_id])?;
        conn.execute("DELETE FROM calendar_dates WHERE service_id = ?;", &[&del_service_id])?;
    }
    debug!("Deleting calendar_dates which are out of time window");
    conn.execute("DELETE FROM calendar_dates WHERE date < ? OR date > ?;",
                 &[&window_start, &window_end])?;
    end(conn)
}

fn delete_empty_routes(conn: &Connection) -> CzttResult<()> {
    begin(conn)?;
    let mut prep_stmt = conn.prepare(
        "SELECT routes.route_id FROM routes
            WHERE 0 = (SELECT COUNT(*) FROM trips WHERE trips.route_id = routes.route_id);")?;
    let delete_route_ids: Vec<String> = prep_stmt.query_map::<String, _, _>(
        &[] as &[&dyn ToSql], |r| r.get::<usize, String>(0))?
        .map(|res| res.expect("Failed to fetch route IDs to delete")).collect();
    for del_route_id in delete_route_ids {
        debug!("Deleting route {} which does not have any trips", &del_route_id);
        conn.execute("DELETE FROM routes WHERE route_id = ?;", &[&del_route_id])?;
    }
    end(conn)
}

fn reindex_agencies(conn: &Connection, reindex_prefix: &str) -> CzttResult<()> {
    begin(conn)?;
    let agency_ids = collect_ids(conn, "agency", "agency_id")?;
    let b64_ids = Base64IdGen::new(reindex_prefix);
    for (old_id, new_id) in agency_ids.iter().zip(b64_ids) {
        debug!("Changing agency ID from {} to {}", &old_id, &new_id);
        replace_ids(conn, "agency", "agency_id", &old_id, &new_id)?;
        replace_ids(conn, "routes", "agency_id", &old_id, &new_id)?;
        replace_ids(conn, "fare_attributes", "agency_id", &old_id, &new_id)?;
    }
    end(conn)
}

fn reindex_stops(conn: &Connection, reindex_prefix: &str) -> CzttResult<()> {
    begin(conn)?;
    let stop_ids = collect_ids(conn, "stops", "stop_id")?;
    let b64_ids = Base64IdGen::new(reindex_prefix);
    for (old_id, new_id) in stop_ids.iter().zip(b64_ids) {
        debug!("Changing stop ID from {} to {}", &old_id, &new_id);
        replace_ids(conn, "stops", "stop_id", &old_id, &new_id)?;
        replace_ids(conn, "stop_times", "stop_id", &old_id, &new_id)?;
        replace_ids(conn, "transfers", "from_stop_id", &old_id, &new_id)?;
        replace_ids(conn, "transfers", "to_stop_id", &old_id, &new_id)?;
    }
    end(conn)
}

fn reindex_routes(conn: &Connection, reindex_prefix: &str) -> CzttResult<()> {
    begin(conn)?;
    let route_ids = collect_ids(conn, "routes", "route_id")?;
    let b64_ids = Base64IdGen::new(reindex_prefix);
    for (old_id, new_id) in route_ids.iter().zip(b64_ids) {
        debug!("Changing route ID from {} to {}", &old_id, &new_id);
        replace_ids(conn, "routes", "route_id", &old_id, &new_id)?;
        replace_ids(conn, "trips", "route_id", &old_id, &new_id)?;
        replace_ids(conn, "fare_rules", "route_id", &old_id, &new_id)?;
    }
    end(conn)
}

fn reindex_trips(conn: &Connection, reindex_prefix: &str) -> CzttResult<()> {
    begin(conn)?;
    let trip_ids = collect_ids(conn, "trips", "trip_id")?;
    let b64_ids = Base64IdGen::new(reindex_prefix);
    for (old_id, new_id) in trip_ids.iter().zip(b64_ids) {
        debug!("Changing trip ID from {} to {}", &old_id, &new_id);
        replace_ids(conn, "trips", "trip_id", &old_id, &new_id)?;
        replace_ids(conn, "stop_times", "trip_id", &old_id, &new_id)?;
        replace_ids(conn, "frequencies", "trip_id", &old_id, &new_id)?;
    }
    end(conn)
}

fn reindex_services(conn: &Connection, reindex_prefix: &str) -> CzttResult<()> {
    begin(conn)?;
    // Find service_ids in trips table, because looking into either
    // calendar or calendar_dates could be incomplete. It's easier to
    // look at trips than merge calendar and calendar_dates results.
    let service_ids = collect_ids(conn, "trips", "service_id")?;
    let b64_ids = Base64IdGen::new(reindex_prefix);
    for (old_id, new_id) in service_ids.iter().zip(b64_ids) {
        debug!("Changing service ID from {} to {}", &old_id, &new_id);
        replace_ids(conn, "calendar", "service_id", &old_id, &new_id)?;
        replace_ids(conn, "calendar_dates", "service_id", &old_id, &new_id)?;
        replace_ids(conn, "trips", "service_id", &old_id, &new_id)?;
    }
    end(conn)
}

fn collect_ids(conn: &Connection, table: &str, id_column: &str) -> CzttResult<Vec<String>> {
    let mut ids = vec![];
    let mut prep_stmt = conn.prepare(&format!(
        "SELECT DISTINCT {} FROM {} ORDER BY {};", id_column, table, id_column))?;
    let mut rows = prep_stmt.query(&[] as &[&dyn ToSql])?;
    while let Some(r) = rows.next()? {
        ids.push(r.get::<usize, String>(0)?);
    }
    Ok(ids)
}

fn replace_ids(conn: &Connection, table: &str, id_column: &str, old_id: &str, new_id: &str)
               -> CzttResult<()> {
    let query = format!("UPDATE {} SET {} = ? WHERE {} = ?;", table, id_column, id_column);
    conn.execute(&query, &[&new_id, &old_id]).map(|_| ()).map_err(|e| e.into())
}


struct Base64IdGen {
    prefix: String,
    current: i32,
}

impl Base64IdGen {
    const BASE64_ID_GEN_OVERFLOW: i32 = 16_777_216;

    pub fn new(prefix: &str) -> Base64IdGen {
        Base64IdGen {
            prefix: prefix.to_string(),
            current: -1,
        }
    }
}

impl Iterator for Base64IdGen {
    type Item = String;

    fn next(&mut self) -> Option<Self::Item> {
        self.current += 1;
        assert!(self.current < Self::BASE64_ID_GEN_OVERFLOW, "Base64IdGen overflow");

        let mut bytes: [u8; 3] = [0; 3];
        bytes[0] = (self.current & 0xff) as u8;
        bytes[1] = ((self.current >> 8) & 0xff) as u8;
        bytes[2] = ((self.current >> 16) & 0xff) as u8;
        Some(format!("{}{}", &self.prefix, base64::encode_config(&bytes, base64::URL_SAFE)))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_base64_id_gen() {
        let mut idg = Base64IdGen::new("BRQ");
        // 0, 0, 0  to  3, 0, 0
        assert_eq!(idg.next().expect("idg returned None").as_str(), "BRQAAAA");
        assert_eq!(idg.next().expect("idg returned None").as_str(), "BRQAQAA");
        assert_eq!(idg.next().expect("idg returned None").as_str(), "BRQAgAA");
        assert_eq!(idg.next().expect("idg returned None").as_str(), "BRQAwAA");

        let mut idg2 = idg.skip(252);
        // 0, 1, 0
        assert_eq!(idg2.next().expect("idg returned None").as_str(), "BRQAAEA");

        let mut idg3 = idg2.skip(1000);
        // 233, 4, 0
        assert_eq!(idg3.next().expect("idg returned None").as_str(), "BRQ6QQA");
    }
}
