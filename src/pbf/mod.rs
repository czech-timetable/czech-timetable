use CzttResult;
use osmpbfreader::{Node, OsmObj, OsmPbfReader};
use rusqlite::{Connection, types::ToSql};
use std::collections::HashMap;
use std::fs::File;
use std::path::Path;

type CoordsMap = HashMap<String, Vec<Coords>>;

struct GtfsStop {
    pub stop_id: String,
    pub stop_name: String,
}

#[derive(Copy, Clone)]
struct Coords {
    pub lat: f64,
    pub lon: f64,
}

pub fn import_stop_coords(conn: &Connection, pbf_path: &Path) -> CzttResult<()> {
    let pbf_file = File::open(&pbf_path)?;
    let mut pbf = OsmPbfReader::new(pbf_file);
    let stops_coords = get_pbf_stop_coords(&mut pbf)?;
    let gtfs_stops = get_gtfs_stops(conn)?;

    for stop in gtfs_stops {
        let coords_opt = lookup_stop_coords(&stops_coords, &stop);
        if let Some(coords) = coords_opt {
            set_gtfs_stop_coords(conn, &stop, &coords)?;
        }
    }

    Ok(())
}

fn set_gtfs_stop_coords(conn: &Connection, stop: &GtfsStop, coords: &Coords) -> CzttResult<()> {
    debug!("Setting coordinates of GTFS stop {}", &stop.stop_name);
    conn.execute("UPDATE stops SET stop_lat = ?, stop_lon = ? WHERE stop_id = ?",
                 &[&coords.lat as &dyn ToSql, &coords.lon, &stop.stop_id])?;
    Ok(())
}

fn get_gtfs_stops(conn: &Connection) -> CzttResult<Vec<GtfsStop>> {
    debug!("Loading GTFS stops.");
    let mut prep_stmt = conn.prepare("SELECT stop_id, stop_name FROM stops;")?;
    let mut rows = prep_stmt.query(&[] as &[&dyn ToSql])?;
    let mut stops = vec![];
    while let Some(r) = rows.next()? {
        stops.push(GtfsStop {
            stop_id: r.get::<usize, Option<String>>(0)?.expect("No stop_id in query result"),
            stop_name: r.get::<usize, Option<String>>(1)?.expect("No stop_name in query result"),
        });
    }
    Ok(stops)
}

fn lookup_stop_coords(map: &CoordsMap, stop: &GtfsStop) -> Option<Coords> {
    // Naive implementation for now, should get smarter in the future,
    // e.g. prepending town name automatically if exact match fails.
    map.get(&stop.stop_name).and_then(|vec| vec.get(0).cloned())
}

fn get_pbf_stop_coords(pbf: &mut OsmPbfReader<File>) -> CzttResult<CoordsMap> {
    debug!("Loading stop coordinates.");
    let mut stop_coords: CoordsMap = CoordsMap::new();

    for obj in pbf.iter().map(Result::unwrap) {
        if ! is_stop_obj(&obj) {
            continue;
        }
        if let Some(node) = obj.node() {
            append_coords(&mut stop_coords, node)
        }
    }

    Ok(stop_coords)
}

fn is_stop_obj(obj: &OsmObj) -> bool {
    obj.is_node() &&
        obj.tags().contains("highway", "bus_stop") ||
        obj.tags().contains("railway", "tram_stop") ||
        obj.tags().contains("railway", "station") ||
        obj.tags().contains("railway", "halt") ||
        obj.tags().contains("public_transport", "stop_position") ||
        obj.tags().contains("public_transport", "station") ||
        obj.tags().contains("amenity", "bus_station") ||
        obj.tags().contains("amenity", "ferry_terminal")
}

fn append_coords(map: &mut CoordsMap, node: &Node) {
    let coords = Coords {
        lat: node.lat(),
        lon: node.lon(),
    };

    fn push_name_coords(map: &mut CoordsMap, name: &str, coords: Coords) {
        if ! map.contains_key(name) {
            map.insert(name.to_string(), vec![]);
        }
        map.get_mut(name)
            .expect("Didn't find coords vec that was just inserted")
            .push(coords);
    }

    if let Some(alt_name) = node.tags.get("alt_name") {
        push_name_coords(map, alt_name, coords.clone());
    }
    if let Some(name) = node.tags.get("name") {
        push_name_coords(map, name, coords);
    }
}
