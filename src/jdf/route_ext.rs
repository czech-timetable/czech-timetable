use CzttResult;
use std::path::{Path, PathBuf};

use super::deser;

#[derive(Debug,Deserialize)]
pub struct RouteExt {
    pub route_id: String,
    pub order: i64,
    pub transport_code: String,
    pub label: String,
    #[serde(deserialize_with = "deser::bool_from_int")]
    pub preferred: bool,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub reserved: Option<String>,
    pub route_sub_id: String,
}

impl RouteExt {
    pub fn from_dir(dir: &Path, _ver_num: &str) -> CzttResult<Vec<RouteExt>> {
        let mut path = PathBuf::from(dir);
        path.push("LinExt.txt");
        // LinExt.txt is not obligatory, graceful exit
        if ! path.exists() {
            return Ok(vec![]);
        }
        Ok(deser::deserialize_csv(&path)?)
    }
}
