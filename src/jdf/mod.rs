use CzttResult;
use std::path::Path;

mod agency;
mod code;
mod csv;
pub mod deser;
mod route;
mod route_ext;
mod route_stop;
mod stop;
mod trip;
mod trip_calendar;
mod trip_stop;
mod validation;
mod version;

pub use self::agency::Agency;
pub use self::code::Code;
pub use self::csv::jdf_file_to_csv_reader;
pub use self::route::Route;
pub use self::route_ext::RouteExt;
pub use self::route_stop::RouteStop;
pub use self::stop::Stop;
pub use self::trip::Trip;
pub use self::trip_calendar::TripCalendar;
pub use self::trip_stop::TripStop;
pub use self::validation::{validate, JdfValidationError};
pub use self::version::Version;

pub struct Jdf {
    pub version: Version,
    pub codes: Vec<Code>,
    pub agencies: Vec<Agency>,
    pub stops: Vec<Stop>,
    pub routes: Vec<Route>,
    pub route_exts: Vec<RouteExt>,
    pub route_stops: Vec<RouteStop>,
    pub trips: Vec<Trip>,
    pub trip_stops: Vec<TripStop>,
    pub trip_calendars: Vec<TripCalendar>,
}

impl Jdf {
    pub fn from_dir(dir: &Path) -> CzttResult<Jdf> {
        let version = Version::from_dir(dir)?;
        let ver_number = version.version.clone();
        let agencies = Agency::from_dir(dir, &ver_number)?;
        let agency_id = match agencies.len() {
            1 => Some(agencies[0].unique_id()),
            _ => None,
        };
        Ok(Jdf {
            version: version,
            codes: Code::from_dir(dir, &ver_number)?,
            agencies: agencies,
            stops: Stop::from_dir(dir, &ver_number, agency_id.as_ref().map(|id| id.as_str()))?,
            routes: Route::from_dir(dir, &ver_number)?,
            route_exts: RouteExt::from_dir(dir, &ver_number)?,
            route_stops: RouteStop::from_dir(dir, &ver_number)?,
            trips: Trip::from_dir(dir, &ver_number)?,
            trip_stops: TripStop::from_dir(dir, &ver_number)?,
            trip_calendars: TripCalendar::from_dir(dir, &ver_number)?,
        })
    }

    // NOTE: the get_* methods could use speeding up by some HashMap cache

    pub fn get_code<'a>(&'a self, code_id: &str) -> Option<&'a Code> {
        for c in &self.codes {
            if c.code_id == code_id {
                return Some(&c)
            }
        }
        None
    }

    pub fn get_agency<'a>(&'a self, agency_id: &str, agency_sub_id: &str)
                          -> Option<&'a Agency> {
        for a in &self.agencies {
            if a.agency_id == agency_id && a.agency_sub_id == agency_sub_id {
                return Some(&a)
            }
        }
        None
    }

    pub fn get_stop<'a>(&'a self, stop_id: &str)
                     -> Option<&'a Stop> {
        for s in &self.stops {
            if s.stop_id == stop_id {
                return Some(&s)
            }
        }
        None
    }

    pub fn get_route<'a>(&'a self, route_id: &str, route_sub_id: &str)
                     -> Option<&'a Route> {
        for r in &self.routes {
            if r.route_id == route_id && r.route_sub_id == route_sub_id {
                return Some(&r)
            }
        }
        None
    }

    pub fn get_route_ext<'a>(&'a self, route_id: &str, route_sub_id: &str)
                             -> Option<&'a RouteExt> {
        for r in &self.route_exts {
            if r.route_id == route_id && r.route_sub_id == route_sub_id && r.preferred {
                return Some(&r)
            }
        }
        for r in &self.route_exts {
            if r.route_id == route_id && r.route_sub_id == route_sub_id {
                return Some(&r)
            }
        }
        None
    }

    pub fn get_trip<'a>(&'a self, route_id: &str, route_sub_id: &str, trip_id: i64)
                     -> Option<&'a Trip> {
        for t in &self.trips {
            if t.route_id == route_id && t.route_sub_id == route_sub_id && t.trip_id == trip_id {
                return Some(&t)
            }
        }
        None
    }

    pub fn get_trip_calendars<'a>(&'a self, route_id: &str, route_sub_id: &str, trip_id: i64)
                     -> Vec<&'a TripCalendar> {
        let mut tcs: Vec<&'a TripCalendar> = vec![];
        for tc in &self.trip_calendars {
            if tc.route_id == route_id && tc.route_sub_id == route_sub_id
                && tc.trip_id == trip_id {
                    // further filter only numeric cal_labels, as
                    // other labels can be used for specifying notes
                    // rather than service date information
                    if i32::from_str_radix(&tc.cal_label, 10).is_ok() {
                        tcs.push(&tc);
                    }
            }
        }
        tcs
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_jdf_simple_1_9() {
        Jdf::from_dir(Path::new("test-data/jdf-simple-1.9"))
            .expect("Error");
    }

    #[test]
    fn test_parse_jdf_simple_1_10() {
        Jdf::from_dir(Path::new("test-data/jdf-simple-1.10"))
            .expect("Error");
    }

    #[test]
    fn test_parse_jdf_simple_1_11() {
        Jdf::from_dir(Path::new("test-data/jdf-simple-1.11"))
            .expect("Error");
    }
}
