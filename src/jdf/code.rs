use CzttResult;
use std::path::{Path, PathBuf};

use super::deser;

#[derive(Debug,Deserialize)]
pub struct Code {
    pub code_id: String,
    pub code_meaning: String,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub reserved: Option<String>,
}

impl Code {
    pub fn from_dir(dir: &Path, _ver_num: &str) -> CzttResult<Vec<Code>> {
        let mut path = PathBuf::from(dir);
        path.push("Pevnykod.txt");
        Ok(deser::deserialize_csv(&path)?)
    }
}
