use CzttResult;
use std::path::{Path, PathBuf};

use super::deser;

#[derive(Debug,Deserialize)]
pub struct Trip {
    pub route_id: String,
    pub trip_id: i64,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr1: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr2: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr3: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr4: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr5: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr6: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr7: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr8: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr9: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr10: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub trip_group_id: Option<String>,
    pub route_sub_id: String,
}

impl Trip {
    pub fn from_dir(dir: &Path, ver_num: &str) -> CzttResult<Vec<Trip>> {
        match ver_num {
            "1.8" | "1.9" => Trip_1_9::from_dir(dir),
            _ => {
                let mut path = PathBuf::from(dir);
                path.push("Spoje.txt");
                Ok(deser::deserialize_csv(&path)?)
            }
        }
    }

    pub fn unique_id(&self, route_unique_id: &str) -> String {
        format!("{}_{}", route_unique_id, &self.trip_id)
    }
}

#[allow(non_camel_case_types)]
#[derive(Debug,Deserialize)]
pub struct Trip_1_9 {
    pub route_id: String,
    pub trip_id: i64,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr1: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr2: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr3: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr4: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr5: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr6: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr7: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr8: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr9: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr10: Option<String>,
}

impl Trip_1_9 {
    pub fn from_dir(dir: &Path) -> CzttResult<Vec<Trip>> {
        let mut path = PathBuf::from(dir);
        path.push("Spoje.txt");
        let records: Vec<Trip_1_9> = deser::deserialize_csv(&path)?;
        Ok(records.into_iter().map(|r| r.to_trip()).collect())
    }

    pub fn to_trip(self) -> Trip {
        Trip {
            route_id: self.route_id,
            trip_id: self.trip_id,
            attr1: self.attr1,
            attr2: self.attr2,
            attr3: self.attr3,
            attr4: self.attr4,
            attr5: self.attr5,
            attr6: self.attr6,
            attr7: self.attr7,
            attr8: self.attr8,
            attr9: self.attr9,
            attr10: self.attr10,
            trip_group_id: None,
            route_sub_id: "1".into(),
        }
    }
}
