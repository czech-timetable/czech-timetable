use base64;
use CzttResult;
use regex::Regex;
use sha1::Sha1;
use std::path::{Path, PathBuf};
use unidecode::unidecode;

use super::deser;

#[derive(Debug,Deserialize)]
pub struct Stop {
    pub stop_id: String,
    pub loc_town: String,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub loc_coarse: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub loc_precise: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub loc_near_town: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub state: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr1: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr2: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr3: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr4: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr5: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr6: Option<String>,

    // Some stops aren't fully qualified (= completely miss town info,
    // usually public transport companies which operate mainly in a
    // single town do this). We'll internally track agency information
    // for purposes of generating fully qualified name (essentially
    // assuming a town name based on agency ID). Needs to be skipped
    // from all Serde operations.
    #[serde(skip)]
    pub agency_id: Option<String>,
}

impl Stop {
    pub fn from_dir(dir: &Path, _ver_num: &str, agency_id: Option<&str>) -> CzttResult<Vec<Stop>> {
        let mut path = PathBuf::from(dir);
        path.push("Zastavky.txt");
        let mut stops: Vec<Stop> = deser::deserialize_csv(&path)?;
        if let Some(id) = agency_id {
            for stop in &mut stops {
                stop.agency_id = Some(id.to_string());
            }
        }
        Ok(stops)
    }

    pub fn readable_name(&self) -> String {
        let loc_coarse_part = match self.loc_coarse {
            Some(ref text) => format!(", {}", text),
            None => String::new(),
        };
        let loc_precise_part = match self.loc_precise {
            Some(ref text) => format!(", {}", text),
            None => String::new(),
        };
        let loc_state_part = match self.state {
            Some(ref text) => format!("({})", text),
            None => String::new(),
        };
        let full_name = format!(
            "{}{}{}{}{}",
            &self.magic_name_part(), &self.loc_town, &loc_coarse_part, &loc_precise_part,
            &loc_state_part);
        let comma_fix = Regex::new(r" *, *").expect("Regex error");
        comma_fix.replace_all(&full_name, ", ").into()
    }

    // This is hardwired magic for agencies which are known to omit
    // town names from stop names. Usually single-town public
    // transport providers.
    fn magic_name_part(&self) -> String {
        match self.agency_id.as_ref().map(|id| id.as_str()) {
            Some("CZ05666112_1") => "Česká Lípa, ".to_string(),
            Some("CZ25013891_1") => "Ústí nad Labem, ".to_string(),
            Some("CZ25166115_1") => "České Budějovice, ".to_string(),
            Some("CZ25220683_1") => "Plzeň, ".to_string(),
            Some("CZ25267213_1") => "Hradec Králové, ".to_string(),
            Some("CZ25320807_1") => "Třebíč, ".to_string(),
            Some("CZ25512897_1") => "Jihlava, ".to_string(),
            Some("CZ26276437_1") => "Kroměříž, ".to_string(),
            Some("CZ26412501_1") => "Mariánské Lázně, ".to_string(),
            Some("CZ47676639_1") => "Olomouc, ".to_string(),
            Some("CZ48364282_1") => "Karlovy Vary, ".to_string(),
            Some("CZ49812947_1") => "Vrchlabí, ".to_string(),
            Some("CZ60071109_1") => "Jindřichův Hradec, ".to_string(),
            Some("CZ60730153_1") => "Zlín, ".to_string(),
            Some("CZ62240935_1") => "Děčín, ".to_string(),
            Some("CZ63217066_1") => "Pardubice, ".to_string(),
            Some("CZ64053466_1") => "Chomutov, ".to_string(),
            Some("CZ64610250_1") => "Opava, ".to_string(),
            Some("CZ70188041_1") => "Havlíčkův Brod, ".to_string(),

            Some(_) | None => String::new(),

            // For the following ids there are some with-town and
            // without-town stop names, seems like all info is in the
            // loc_town field either way though. Hard to make sense of
            // it, not possible to just prepend something to fix it.
            // Some("CZ25827405_1") => ", ".to_string(),
            // Some("CZ25945408_1") => ", ".to_string(),
            // Some("CZ26060451_1") => ", ".to_string(),
            // Some("CZ45192120_1") => ", ".to_string(),
            // Some("CZ46345850_1") => ", ".to_string(),
            // Some("CZ46965815_1") => ", ".to_string(),
            // Some("CZ48362611_1") => ", ".to_string(),
        }
    }

    pub fn unique_id(&self) -> String {
        let hash_src = unidecode(&self.readable_name()).to_lowercase();
        // take 12 chars of base64 => 9 bytes, equivalent of 18 chars of hexdigest
        base64::encode(&Sha1::from(&hash_src).digest().bytes())[..12].to_string()
    }
}
