use chrono::NaiveDate;
use CzttResult;
use std::path::{Path, PathBuf};

use super::deser;

#[derive(Debug,Deserialize)]
pub struct Version {
    pub version: String,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub du_number: Option<i32>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub region: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub batch_id: Option<String>,
    #[serde(deserialize_with = "deser::naive_date")]
    pub created_date: NaiveDate,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub name: Option<String>,
}

impl Version {
    pub fn from_dir(dir: &Path) -> CzttResult<Version> {
        let v1_9 = Version_1_9::from_dir(dir)?;
        let version = if v1_9.is_some() {
            v1_9.expect("Failed unwrap of JDF 1.9 version info.")
        } else {
            let mut path = PathBuf::new();
            path.push(dir);
            path.push("VerzeJDF.txt");
            let mut records: Vec<Version> = deser::deserialize_csv(&path)?;
            if records.len() != 1 {
                return Err(format!(
                    "Expected a single VerzeJDF record but got {}", records.len()).into())
            }
            records.pop().expect("Failed to pop Version from Vec.")
        };

        if ! ["1.8", "1.9", "1.10", "1.11"].contains(&version.version.as_str()) {
            return Err(format!(
                "Unsupported JDF version {} in {:?}", version.version, dir).into())
        }
        Ok(version)
    }
}

#[allow(non_camel_case_types)]
#[derive(Debug,Deserialize)]
pub struct Version_1_9 {
    pub version: String,
}

impl Version_1_9 {
    pub fn from_dir(dir: &Path) -> CzttResult<Option<Version>> {
        let mut path = PathBuf::new();
        path.push(dir);
        path.push("VerzeJDF.txt");
        let mut records: Vec<Version_1_9> = deser::deserialize_csv(&path)?;
        if records.len() != 1 {
            return Err(format!(
                "Expected a single VerzeJDF record but got {}", records.len()).into())
        }
        let row = records.pop().expect("Failed to pop Version from Vec.");

        if ["1.8", "1.9"].contains(&row.version.as_str()) {
            Ok(Some(Version {
                version: row.version,
                du_number: None,
                region: None,
                batch_id: None,
                created_date: NaiveDate::from_ymd(1900,1,1),
                name: None,
            }))
        } else {
            Ok(None)
        }
    }
}
