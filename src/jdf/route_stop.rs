use CzttResult;
use std::path::{Path, PathBuf};

use super::deser;

#[derive(Debug,Deserialize)]
pub struct RouteStop {
    pub route_id: String,
    pub rate_number: i64,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub rate_zone: Option<String>,
    pub stop_id: String,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub avg_time: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr1: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr2: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr3: Option<String>,
    pub route_sub_id: String,
}

impl RouteStop {
    pub fn from_dir(dir: &Path, ver_num: &str) -> CzttResult<Vec<RouteStop>> {
        match ver_num {
            "1.8" | "1.9" => RouteStop_1_9::from_dir(dir),
            _ => {
                let mut path = PathBuf::from(dir);
                path.push("Zaslinky.txt");
                Ok(deser::deserialize_csv(&path)?)
            }
        }
    }
}

#[allow(non_camel_case_types)]
#[derive(Debug,Deserialize)]
pub struct RouteStop_1_9 {
    pub route_id: String,
    pub rate_number: i64,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub rate_zone: Option<String>,
    pub stop_id: String,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr1: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr2: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr3: Option<String>,
}

impl RouteStop_1_9 {
    pub fn from_dir(dir: &Path) -> CzttResult<Vec<RouteStop>> {
        let mut path = PathBuf::from(dir);
        path.push("Zaslinky.txt");
        let records: Vec<RouteStop_1_9> = deser::deserialize_csv(&path)?;
        Ok(records.into_iter().map(|r| r.to_route_stop()).collect())
    }

    pub fn to_route_stop(self) -> RouteStop {
        RouteStop {
            route_id: self.route_id,
            rate_number: self.rate_number,
            rate_zone: self.rate_zone,
            stop_id: self.stop_id,
            avg_time: None,
            attr1: self.attr1,
            attr2: self.attr2,
            attr3: self.attr3,
            route_sub_id: "1".into(),
        }
    }
}
