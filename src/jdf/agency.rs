use CzttResult;
use std::path::{Path, PathBuf};

use super::deser;

#[derive(Debug,Deserialize)]
pub struct Agency {
    pub agency_id: String,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub dic: Option<String>,
    pub name: String,
    pub company_type: u64,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub person_name: Option<String>,
    pub address: String,
    pub phone: String,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub control_phone: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub info_phone: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub fax: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub email: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub www: Option<String>,
    pub agency_sub_id: String,
}

impl Agency {
    pub fn from_dir(dir: &Path, ver_num: &str) -> CzttResult<Vec<Agency>> {
        match ver_num {
            "1.8" | "1.9" => Agency_1_9::from_dir(dir),
            _ => {
                let mut path = PathBuf::from(dir);
                path.push("Dopravci.txt");
                Ok(deser::deserialize_csv(&path)?)
            }
        }
    }


    pub fn unique_id(&self) -> String {
        format!("CZ{}_{}", &self.agency_id, &self.agency_sub_id)
    }
}

#[allow(non_camel_case_types)]
#[derive(Debug,Deserialize)]
pub struct Agency_1_9 {
    pub agency_id: String,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub dic: Option<String>,
    pub name: String,
    pub company_type: u64,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub person_name: Option<String>,
    pub address: String,
    pub phone: String,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub control_phone: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub info_phone: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub fax: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub email: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub www: Option<String>,
}

impl Agency_1_9 {
    pub fn from_dir(dir: &Path) -> CzttResult<Vec<Agency>> {
        let mut path = PathBuf::from(dir);
        path.push("Dopravci.txt");
        let records: Vec<Agency_1_9> = deser::deserialize_csv(&path)?;
        Ok(records.into_iter().map(|r| r.to_agency()).collect())
    }

    pub fn to_agency(self) -> Agency {
        Agency {
            agency_id: self.agency_id,
            dic: self.dic,
            name: self.name,
            company_type: self.company_type,
            person_name: self.person_name,
            address: self.address,
            phone: self.phone,
            control_phone: self.control_phone,
            info_phone: self.info_phone,
            fax: self.fax,
            email: self.email,
            www: self.www,
            agency_sub_id: "1".into(),
        }
    }
}
