use chrono::NaiveDate;
use CzttResult;
use std::path::{Path, PathBuf};

use super::deser;

#[derive(Debug,Deserialize)]
pub struct TripCalendar {
    pub route_id: String,
    pub trip_id: i64,
    pub cal_id: String,
    pub cal_label: String,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub cal_type: Option<String>,
    #[serde(deserialize_with = "deser::option_naive_date")]
    pub from_date: Option<NaiveDate>,
    #[serde(deserialize_with = "deser::option_naive_date")]
    pub to_date: Option<NaiveDate>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub note: Option<String>,
    pub route_sub_id: String,
}

impl TripCalendar {
    pub fn from_dir(dir: &Path, ver_num: &str) -> CzttResult<Vec<TripCalendar>> {
        match ver_num {
            "1.8" | "1.9" => TripCalendar_1_9::from_dir(dir),
            _ => {
                let mut path = PathBuf::from(dir);
                path.push("Caskody.txt");
                Ok(deser::deserialize_csv(&path)?)
            }
        }
    }
}

#[allow(non_camel_case_types)]
#[derive(Debug,Deserialize)]
pub struct TripCalendar_1_9 {
    pub route_id: String,
    pub trip_id: i64,
    pub cal_id: String,
    pub cal_label: String,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub cal_type: Option<String>,
    #[serde(deserialize_with = "deser::option_naive_date")]
    pub from_date: Option<NaiveDate>,
    #[serde(deserialize_with = "deser::option_naive_date")]
    pub to_date: Option<NaiveDate>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub note: Option<String>,
}

impl TripCalendar_1_9 {
    pub fn from_dir(dir: &Path) -> CzttResult<Vec<TripCalendar>> {
        let mut path = PathBuf::from(dir);
        path.push("Caskody.txt");
        let records: Vec<TripCalendar_1_9> = deser::deserialize_csv(&path)?;
        Ok(records.into_iter().map(|r| r.to_trip_calendar()).collect())
    }

    pub fn to_trip_calendar(self) -> TripCalendar {
        TripCalendar {
            route_id: self.route_id,
            trip_id: self.trip_id,
            cal_id: self.cal_id,
            cal_label: self.cal_label,
            cal_type: self.cal_type,
            from_date: self.from_date,
            to_date: self.to_date,
            note: self.note,
            route_sub_id: "1".into(),
        }
    }
}
