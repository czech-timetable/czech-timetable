use chrono::NaiveDate;
use CzttResult;
use std::path::{Path, PathBuf};

use super::deser;

#[derive(Debug,Deserialize)]
pub struct Route {
    pub route_id: String,
    pub name: String,
    pub agency_id: String,
    pub route_type: String,
    pub vehicle_type: String,
    #[serde(deserialize_with = "deser::bool_from_int")]
    pub is_detour: bool,
    #[serde(deserialize_with = "deser::bool_from_int")]
    pub is_group: bool,
    #[serde(deserialize_with = "deser::bool_from_int")]
    pub has_labels: bool,
    #[serde(deserialize_with = "deser::bool_from_int")]
    pub is_oneway: bool,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub reserved: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub licence_num: Option<String>,
    #[serde(deserialize_with = "deser::option_naive_date")]
    pub licence_valid_from: Option<NaiveDate>,
    #[serde(deserialize_with = "deser::option_naive_date")]
    pub licence_valid_to: Option<NaiveDate>,
    #[serde(deserialize_with = "deser::naive_date")]
    pub valid_from: NaiveDate,
    #[serde(deserialize_with = "deser::naive_date")]
    pub valid_to: NaiveDate,
    pub agency_sub_id: String,
    pub route_sub_id: String,
}

impl Route {
    pub fn from_dir(dir: &Path, ver_num: &str) -> CzttResult<Vec<Route>> {
        match ver_num {
            "1.8" | "1.9" => Route_1_9::from_dir(dir),
            "1.10" => Route_1_10::from_dir(dir),
            _ => {
                let mut path = PathBuf::from(dir);
                path.push("Linky.txt");
                Ok(deser::deserialize_csv(&path)?)
            }
        }
    }

    pub fn unique_id(&self) -> String {
        format!("CZ{}_{}_{}", &self.route_id, &self.route_sub_id,
                &self.valid_from.format("%y%m%d").to_string())
    }
}

#[allow(non_camel_case_types)]
#[derive(Debug,Deserialize)]
pub struct Route_1_10 {
    pub route_id: String,
    pub name: String,
    pub agency_id: String,
    pub route_type: String,
    pub vehicle_type: String,
    #[serde(deserialize_with = "deser::bool_from_int")]
    pub is_detour: bool,
    #[serde(deserialize_with = "deser::bool_from_int")]
    pub is_group: bool,
    #[serde(deserialize_with = "deser::bool_from_int")]
    pub has_labels: bool,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub reserved: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub licence_num: Option<String>,
    #[serde(deserialize_with = "deser::option_naive_date")]
    pub licence_valid_from: Option<NaiveDate>,
    #[serde(deserialize_with = "deser::option_naive_date")]
    pub licence_valid_to: Option<NaiveDate>,
    #[serde(deserialize_with = "deser::naive_date")]
    pub valid_from: NaiveDate,
    #[serde(deserialize_with = "deser::naive_date")]
    pub valid_to: NaiveDate,
    pub agency_sub_id: String,
    pub route_sub_id: String,
}

impl Route_1_10 {
    pub fn from_dir(dir: &Path) -> CzttResult<Vec<Route>> {
        let mut path = PathBuf::from(dir);
        path.push("Linky.txt");
        let records: Vec<Route_1_10> = deser::deserialize_csv(&path)?;
        Ok(records.into_iter().map(|r| r.to_route()).collect())
    }

    pub fn to_route(self) -> Route {
        Route {
            route_id: self.route_id,
            name: self.name,
            agency_id: self.agency_id,
            route_type: self.route_type,
            vehicle_type: self.vehicle_type,
            is_detour: self.is_detour,
            is_group: self.is_group,
            has_labels: self.has_labels,
            is_oneway: false,
            reserved: self.reserved,
            licence_num: self.licence_num,
            licence_valid_from: self.licence_valid_from,
            licence_valid_to: self.licence_valid_to,
            valid_from: self.valid_from,
            valid_to: self.valid_to,
            agency_sub_id: self.agency_sub_id,
            route_sub_id: self.route_sub_id,
        }
    }
}

#[allow(non_camel_case_types)]
#[derive(Debug,Deserialize)]
pub struct Route_1_9 {
    pub route_id: String,
    pub name: String,
    pub agency_id: String,
    pub route_type: String,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub reserved: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub licence_num: Option<String>,
    #[serde(deserialize_with = "deser::option_naive_date")]
    pub licence_valid_from: Option<NaiveDate>,
    #[serde(deserialize_with = "deser::option_naive_date")]
    pub licence_valid_to: Option<NaiveDate>,
    #[serde(deserialize_with = "deser::naive_date")]
    pub valid_from: NaiveDate,
    #[serde(deserialize_with = "deser::naive_date")]
    pub valid_to: NaiveDate,
}

impl Route_1_9 {
    pub fn from_dir(dir: &Path) -> CzttResult<Vec<Route>> {
        let mut path = PathBuf::from(dir);
        path.push("Linky.txt");
        let records: Vec<Route_1_9> = deser::deserialize_csv(&path)?;
        Ok(records.into_iter().map(|r| r.to_route()).collect())
    }

    pub fn to_route(self) -> Route {
        Route {
            route_id: self.route_id,
            name: self.name,
            agency_id: self.agency_id,
            route_type: self.route_type,
            vehicle_type: "A".into(),
            is_detour: false,
            is_group: false,
            has_labels: false,
            is_oneway: false,
            reserved: self.reserved,
            licence_num: self.licence_num,
            licence_valid_from: self.licence_valid_from,
            licence_valid_to: self.licence_valid_to,
            valid_from: self.valid_from,
            valid_to: self.valid_to,
            agency_sub_id: "1".into(),
            route_sub_id: "1".into(),
        }
    }
}
