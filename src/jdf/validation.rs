use super::Jdf;

pub struct JdfValidationError {
    pub message: String,
}

pub fn validate(jdf: &Jdf) -> Vec<JdfValidationError> {
    let mut errors: Vec<JdfValidationError> = vec![];
    errors.append(&mut validate_stop_names(jdf));
    errors
}

fn validate_stop_names(jdf: &Jdf) -> Vec<JdfValidationError> {
    let mut errors: Vec<JdfValidationError> = vec![];
    for stop in &jdf.stops {
        // if we have a multiple component name, assume stop name is
        // fully qualified
        if stop.loc_coarse.is_some() || stop.loc_precise.is_some() {
            continue;
        }

        if let Some(ref near_town) = stop.loc_near_town {
            if &stop.loc_town != near_town {
                let msg = format!(
                    "Stop '{}' of agency '{:?}' is single component \
                     and it's not its town name '{}'.",
                    stop.readable_name(), stop.agency_id, near_town);
                errors.push(JdfValidationError { message: msg });
                }
        } else {
            let msg = format!(
                "Stop '{}' of agency '{:?}' has single component name and misses near town info.",
                stop.readable_name(), stop.agency_id);
            errors.push(JdfValidationError { message: msg });
        }
    }
    errors
}
