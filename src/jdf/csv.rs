use csv;
use CzttResult;
use encoding_rs::WINDOWS_1250;
use std::io::{Cursor, Read};
use std::fs::File;
use std::path::Path;

const INITIAL_CSV_BUF_CAP: usize = 4096;

pub type CsvData = Cursor<Vec<u8>>;
pub type CsvReader = csv::Reader<CsvData>;

pub fn jdf_file_to_csv_reader(path: &Path) -> CzttResult<CsvReader> {
    let csv_string = jdf_file_to_csv(&path)?;
    let csv_bytes: Vec<u8> = csv_string.into_bytes();
    Ok(csv::ReaderBuilder::new()
        .has_headers(false)
        .from_reader(Cursor::new(csv_bytes)))
}

pub fn jdf_file_to_csv(path: &Path) -> CzttResult<String> {
    let mut f = File::open(path).expect(&format!("Failed to open file {:?}", path));
    let mut buf: Vec<u8> = Vec::with_capacity(INITIAL_CSV_BUF_CAP);
    f.read_to_end(&mut buf)?;
    jdf_bytes_to_csv(&buf)
}

pub fn jdf_bytes_to_csv(bytes: &[u8]) -> CzttResult<String> {
    let (cow, enc_used, had_errors) = WINDOWS_1250.decode(bytes);
    if had_errors {
        return Err("Couldn't convert WINDOWS_1250 string to utf8 without errors.".into())
    }
    assert!(enc_used == WINDOWS_1250, "Failed to decode file as WINDOWS_1250.");

    let slice: &str = &cow;
    Ok(slice.replace(";\r\n", "\n"))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_jdf_bytes_to_csv() {
        let src =
            b"\"123\",\"\xC8\xE1pkova\";\r\n\"456\",\"Kone\xE8n\xE9ho n\xE1m\xECst\xED\";\r\n";
        let exp = "\"123\",\"Čápkova\"\n\"456\",\"Konečného náměstí\"\n";
        assert_eq!(exp, jdf_bytes_to_csv(src).expect("Error"));
    }
}
