use chrono::{NaiveDate, NaiveTime};
use CzttResult;
use jdf::jdf_file_to_csv_reader;
use serde::{self, Deserialize, Deserializer};
use std::error::Error;
use std::path::Path;
use std::str::FromStr;

const DATE_FORMAT: &'static str = "%d%m%Y";
const TIME_FORMAT: &'static str = "%H%M";

pub fn deserialize_csv<T>(path: &Path)
                          -> CzttResult<Vec<T>>
where for<'de> T: Deserialize<'de> {
    let mut reader = jdf_file_to_csv_reader(&path)?;
    let mut records: Vec<T> = vec![];
    for rec_result in reader.deserialize() {
        records.push(rec_result.map_err(
            |e| format!("Error while deserializing {:?}: {:?}", path, e))?);
    }
    Ok(records)
}

pub fn bool_from_int<'de, D>(deserializer: D) -> Result<bool, D::Error>
where D: Deserializer<'de> {
    let s = String::deserialize(deserializer)?;
    let integer = i64::from_str(&s).map_err(serde::de::Error::custom)?;
    Ok(integer != 0)
}

pub fn option_from_str<'de, D, T, E>(deserializer: D) -> Result<Option<T>, D::Error>
where D: Deserializer<'de>,
      T: FromStr<Err = E>,
      E: Error {
    let s = String::deserialize(deserializer)?;
    if s.len() == 0 {
        return Ok(None)
    }
    T::from_str(&s).map_err(serde::de::Error::custom).map(|result| Some(result))
}

pub fn naive_date<'de, D>(deserializer: D) -> Result<NaiveDate, D::Error>
where D: Deserializer<'de> {
    let s = String::deserialize(deserializer)?;
    NaiveDate::parse_from_str(&s, DATE_FORMAT).map_err(serde::de::Error::custom)
}

pub fn option_naive_date<'de, D>(deserializer: D) -> Result<Option<NaiveDate>, D::Error>
where D: Deserializer<'de> {
    let s = String::deserialize(deserializer)?;
    if s.len() == 0 {
        return Ok(None);
    }
    NaiveDate::parse_from_str(&s, DATE_FORMAT)
        .map_err(serde::de::Error::custom)
        .map(|date| Some(date))
}

pub fn option_naive_time<'de, D>(deserializer: D) -> Result<Option<NaiveTime>, D::Error>
where D: Deserializer<'de> {
    let s = String::deserialize(deserializer)?;
    // < and | are special chars for "this stop is only serviced in
    // the other direction" and "vehicle is just passing through", i
    // think both mean that there is no getting on/off the vehicle in
    // that stop, so we can probably just map to None
    if s.len() == 0 || &s == "<" || &s == "|" {
        return Ok(None);
    }
    NaiveTime::parse_from_str(&s, TIME_FORMAT)
        .map_err(serde::de::Error::custom)
        .map(|date| Some(date))
}
