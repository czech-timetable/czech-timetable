use chrono::NaiveTime;
use CzttResult;
use std::path::{Path, PathBuf};

use super::deser;

#[derive(Debug,Deserialize)]
pub struct TripStop {
    pub route_id: String,
    pub trip_id: i64,
    pub rate_number: i64,
    pub stop_id: String,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub label_id: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub platform_number: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr1: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr2: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr3: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub kilometers: Option<f32>,
    #[serde(deserialize_with = "deser::option_naive_time")]
    pub arrival_time: Option<NaiveTime>,
    #[serde(deserialize_with = "deser::option_naive_time")]
    pub departure_time: Option<NaiveTime>,
    #[serde(deserialize_with = "deser::option_naive_time")]
    pub min_arrival_time: Option<NaiveTime>,
    #[serde(deserialize_with = "deser::option_naive_time")]
    pub max_departure_time: Option<NaiveTime>,
    pub route_sub_id: String,
}

impl TripStop {
    pub fn from_dir(dir: &Path, ver_num: &str) -> CzttResult<Vec<TripStop>> {
        match ver_num {
            "1.8" | "1.9" => TripStop_1_9::from_dir(dir),
            "1.10" => TripStop_1_10::from_dir(dir),
            _ => {
                let mut path = PathBuf::from(dir);
                path.push("Zasspoje.txt");
                Ok(deser::deserialize_csv(&path)?)
            }
        }
    }
}

#[allow(non_camel_case_types)]
#[derive(Debug,Deserialize)]
pub struct TripStop_1_10 {
    pub route_id: String,
    pub trip_id: i64,
    pub rate_number: i64,
    pub stop_id: String,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub label_id: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub platform_number: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr1: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr2: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub kilometers: Option<f32>,
    #[serde(deserialize_with = "deser::option_naive_time")]
    pub arrival_time: Option<NaiveTime>,
    #[serde(deserialize_with = "deser::option_naive_time")]
    pub departure_time: Option<NaiveTime>,
    pub route_sub_id: String,
}

impl TripStop_1_10 {
    pub fn from_dir(dir: &Path) -> CzttResult<Vec<TripStop>> {
        let mut path = PathBuf::from(dir);
        path.push("Zasspoje.txt");
        let records: Vec<TripStop_1_10> = deser::deserialize_csv(&path)?;
        Ok(records.into_iter().map(|r| r.to_trip_stop()).collect())
    }

    pub fn to_trip_stop(self) -> TripStop {
        TripStop {
            route_id: self.route_id,
            trip_id: self.trip_id,
            rate_number: self.rate_number,
            stop_id: self.stop_id,
            label_id: self.label_id,
            platform_number: self.platform_number,
            attr1: self.attr1,
            attr2: self.attr2,
            attr3: None,
            kilometers: self.kilometers,
            arrival_time: self.arrival_time,
            departure_time: self.departure_time,
            min_arrival_time: None,
            max_departure_time: None,
            route_sub_id: self.route_sub_id,
        }
    }
}

#[allow(non_camel_case_types)]
#[derive(Debug,Deserialize)]
pub struct TripStop_1_9 {
    pub route_id: String,
    pub trip_id: i64,
    pub rate_number: i64,
    pub stop_id: String,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub platform_number: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr1: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub attr2: Option<String>,
    #[serde(deserialize_with = "deser::option_from_str")]
    pub kilometers: Option<f32>,
    #[serde(deserialize_with = "deser::option_naive_time")]
    pub arrival_time: Option<NaiveTime>,
    #[serde(deserialize_with = "deser::option_naive_time")]
    pub departure_time: Option<NaiveTime>,
}

impl TripStop_1_9 {
    pub fn from_dir(dir: &Path) -> CzttResult<Vec<TripStop>> {
        let mut path = PathBuf::from(dir);
        path.push("Zasspoje.txt");
        let records: Vec<TripStop_1_9> = deser::deserialize_csv(&path)?;
        Ok(records.into_iter().map(|r| r.to_trip_stop()).collect())
    }

    pub fn to_trip_stop(self) -> TripStop {
        TripStop {
            route_id: self.route_id,
            trip_id: self.trip_id,
            rate_number: self.rate_number,
            stop_id: self.stop_id,
            label_id: None,
            platform_number: self.platform_number,
            attr1: self.attr1,
            attr2: self.attr2,
            attr3: None,
            kilometers: self.kilometers,
            arrival_time: self.arrival_time,
            departure_time: self.departure_time,
            min_arrival_time: None,
            max_departure_time: None,
            route_sub_id: "1".into(),
        }
    }
}
