extern crate clap;
extern crate czech_timetable;
extern crate log;
extern crate stderrlog;

use clap::{Arg, ArgMatches, App, AppSettings, SubCommand};
use czech_timetable::{
    CzttResult,
    jdf::{validate, Jdf},
    pbf,
    gtfs::jdf_import,
    gtfs::export::export,
    gtfs::minify::minify,
    sqlite
};
use std::path::PathBuf;
use std::process::exit;

fn main() {
    let matches = App::new("Czech Timetable")
        .setting(AppSettings::SubcommandRequiredElseHelp)
        .about("Convert Czech open data public transport timetables\n \
                from Czech gov formats to GTFS format")
        .arg(Arg::with_name("debug")
             .long("debug")
             .help("Enable debugging output"))
        .subcommand(SubCommand::with_name("create-db")
                    .about("Create fresh SQLite database for processing timetable data")
                    .arg(Arg::with_name("db")
                         .long("db")
                         .required(true)
                         .takes_value(true)
                         .value_name("FILE")
                         .help("Path to SQLite DB file")))
        .subcommand(SubCommand::with_name("export-gtfs")
                    .about("Export the database as GTFS CSV files")
                    .arg(Arg::with_name("db")
                         .long("db")
                         .required(true)
                         .takes_value(true)
                         .value_name("FILE")
                         .help("Path to SQLite DB file"))
                    .arg(Arg::with_name("gtfs_dir")
                         .long("gtfs-dir")
                         .required(true)
                         .takes_value(true)
                         .value_name("GTFS_DIR")
                         .help("Path to GTFS directory"))
                    .arg(Arg::with_name("agency")
                         .long("agency")
                         .takes_value(true)
                         .multiple(true)
                         .value_name("AGENCY_ID")
                         .help("Agency ID to export. Can be given multiple times. \
                                If not given, export everything.")))
        .subcommand(SubCommand::with_name("minify-db")
                    .about("Minify SQLite (shorten IDs, drop obsolete data etc.)")
                    .arg(Arg::with_name("db")
                         .long("db")
                         .required(true)
                         .takes_value(true)
                         .value_name("FILE")
                         .help("Path to SQLite DB file"))
                    .arg(Arg::with_name("no_reindex")
                         .long("no-reindex")
                         .help("Disable index shrinking"))
                    .arg(Arg::with_name("reindex_agencies")
                         .long("reindex-agencies")
                         .help("When reindexing, reindex also agencies"))
                    .arg(Arg::with_name("reindex_prefix")
                         .long("reindex-prefix")
                         .takes_value(true)
                         .default_value("")
                         .value_name("PREFIX")
                         .help("Prefix to apply to all reindexed IDs"))
                    .arg(Arg::with_name("no_time_limits")
                         .long("no-time-limits")
                         .help("Disable deleting records too far in history or future"))
                    .arg(Arg::with_name("max_history")
                         .long("max-history")
                         .takes_value(true)
                         .default_value("30")
                         .value_name("DAYS")
                         .help("Records concerning dates more than DAYS ago will be removed"))
                    .arg(Arg::with_name("max_future")
                         .long("max-future")
                         .takes_value(true)
                         .default_value("180")
                         .value_name("DAYS")
                         .help("Records concerning dates more than DAYS ago will be removed"))
        )
        .subcommand(SubCommand::with_name("import-jdf")
                    .about("Import a single JDF into SQLite database")
                    .arg(Arg::with_name("db")
                         .long("db")
                         .required(true)
                         .takes_value(true)
                         .value_name("FILE")
                         .help("Path to SQLite DB file"))
                    .arg(Arg::with_name("jdf_dir")
                         .long("jdf-dir")
                         .required(true)
                         .takes_value(true)
                         .value_name("JDF_DIR")
                         .help("Path to JDF directory"))
                    .arg(Arg::with_name("agency")
                         .long("agency")
                         .takes_value(true)
                         .multiple(true)
                         .value_name("AGENCY_ID")
                         .help("Agency ID to import. Can be given multiple times. \
                                If not given, import everything.")))
        .subcommand(SubCommand::with_name("import-pbf-stop-coords")
                    .about("Import stop coordinates from OpenStreetMap PBF file")
                    .arg(Arg::with_name("db")
                         .long("db")
                         .required(true)
                         .takes_value(true)
                         .value_name("FILE")
                         .help("Path to SQLite DB file"))
                    .arg(Arg::with_name("pbf")
                         .long("pbf")
                         .required(true)
                         .takes_value(true)
                         .value_name("PBF")
                         .help("Path to OpenStreetMap PBF file")))
        .subcommand(SubCommand::with_name("validate-jdf")
                    .about("Validate JDF directory")
                    .arg(Arg::with_name("jdf_dir")
                         .long("jdf-dir")
                         .required(true)
                         .takes_value(true)
                         .value_name("JDF_DIR")
                         .help("Path to JDF directory")))
        .get_matches();

    let mut logger = stderrlog::new();
    if matches.is_present("debug") {
        logger.verbosity(3);  // maps to LevelFilter::Debug
    } else {
        logger.verbosity(2);  // maps to LevelFilter::Info
    }
    logger.init().expect("Error when trying to set up logging");

    if matches.subcommand_name().is_none() {
        println!("{}", matches.usage());
        exit(1);
    }

    let res = match matches.subcommand() {
        ("create-db", Some(sub_matches)) => create_db(&sub_matches),
        ("minify-db", Some(sub_matches)) => minify_db(&sub_matches),
        ("export-gtfs", Some(sub_matches)) => export_gtfs(&sub_matches),
        ("import-jdf", Some(sub_matches)) => import_jdf(&sub_matches),
        ("import-pbf-stop-coords", Some(sub_matches)) => import_pbf_stop_coords(&sub_matches),
        ("validate-jdf", Some(sub_matches)) => validate_jdf(&sub_matches),
        _ => { Err("Unknown subcommand.".into()) },
    };

    match res {
        Ok(_) => {},
        Err(e) => { eprintln!("Error: {}", e); exit(1) },
    };
}

fn create_db(sub_matches: &ArgMatches) -> CzttResult<()> {
    let db_path_arg = sub_matches.value_of("db")
        .expect("Required argument --db not provided, should have been caught elsewhere.");
    sqlite::create_db(&PathBuf::from(&db_path_arg))
}

fn minify_db(sub_matches: &ArgMatches) -> CzttResult<()> {
    let db_path_arg = sub_matches.value_of("db")
        .expect("Required argument --db not provided, should have been caught elsewhere.");
    let reindex = sub_matches.value_of("no_reindex").is_none();
    let reindex_agencies = sub_matches.value_of("reindex_agencies").is_some();
    let reindex_prefix = sub_matches.value_of("reindex_prefix")
        .expect("Missing --reindex-prefix parameter value");
    let time_limits = sub_matches.value_of("no_time_limits").is_none();
    let max_history = u32::from_str_radix(
        sub_matches.value_of("max_history").expect("Missing --max-history value"), 10)
        .expect("Failed to parse max_history as non-negative integer");
    let max_future = u32::from_str_radix(
        sub_matches.value_of("max_future").expect("Missing --max-future value"), 10)
        .expect("Failed to parse max_future as non-negative integer");

    let conn = sqlite::connect(&PathBuf::from(&db_path_arg))?;
    minify(&conn, reindex, reindex_agencies, reindex_prefix, time_limits, max_history, max_future)
}

fn export_gtfs(sub_matches: &ArgMatches) -> CzttResult<()> {
    let db_path_arg = sub_matches.value_of("db")
        .expect("Required argument --db not provided, should have been caught elsewhere.");
    let jdf_dir_arg = sub_matches.value_of("gtfs_dir")
        .expect("Required argument --gtfs-dir not provided, should have been caught elsewhere.");
    let agency_ids = sub_matches.values_of_lossy("agency");

    let conn = sqlite::connect(&PathBuf::from(&db_path_arg))?;
    export(&conn, &PathBuf::from(&jdf_dir_arg), agency_ids)
}

fn import_jdf(sub_matches: &ArgMatches) -> CzttResult<()> {
    let db_path_arg = sub_matches.value_of("db")
        .expect("Required argument --db not provided, should have been caught elsewhere.");
    let jdf_dir_arg = sub_matches.value_of("jdf_dir")
        .expect("Required argument --jdf-dir not provided, should have been caught elsewhere.");
    let agency_ids = sub_matches.values_of_lossy("agency");

    let jdf = Jdf::from_dir(&PathBuf::from(&jdf_dir_arg))?;
    let conn = sqlite::connect(&PathBuf::from(&db_path_arg))?;
    jdf_import::import_jdf(&conn, &jdf, agency_ids)
}

fn import_pbf_stop_coords(sub_matches: &ArgMatches) -> CzttResult<()> {
    let db_path_arg = sub_matches.value_of("db")
        .expect("Required argument --db not provided, should have been caught elsewhere.");
    let pbf_arg = sub_matches.value_of("pbf")
        .expect("Required argument --pbf not provided, should have been caught elsewhere.");

    let conn = sqlite::connect(&PathBuf::from(&db_path_arg))?;
    pbf::import_stop_coords(&conn, &PathBuf::from(&pbf_arg))
}

fn validate_jdf(sub_matches: &ArgMatches) -> CzttResult<()> {
    let jdf_dir_arg = sub_matches.value_of("jdf_dir")
        .expect("Required argument --jdf-dir not provided, should have been caught elsewhere.");

    let jdf = Jdf::from_dir(&PathBuf::from(&jdf_dir_arg))?;
    let errors = validate(&jdf);
    match errors.len() {
        0 => Ok(()),
        _ => {
            for e in errors {
                println!("Error: {}", e.message);
            }
            Err("Validation failed".into())
        },
    }
}
