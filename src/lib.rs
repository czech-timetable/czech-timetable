#[macro_use]
extern crate log;
#[macro_use]
extern crate serde_derive;

extern crate base64;
extern crate chrono;
extern crate csv;
extern crate encoding_rs;
extern crate holiday_cz;
extern crate osmpbfreader;
extern crate regex;
extern crate rusqlite;
extern crate serde;
extern crate sha1;
extern crate unidecode;

pub mod jdf;
pub mod gtfs;
pub mod pbf;
pub mod sqlite;

pub type CzttResult<T> = Result<T, Box<dyn std::error::Error>>;
